import test from "ava";

test.beforeEach(async t =>
  import("./DataSource.js").then(
    ({ DataSource }) => (t.context.DataSource = DataSource)
  )
);

test("exports {DataSource}", async t => {
  t.truthy(t.context.DataSource);
});
test("constructor()", async t => {
  const ds = new t.context.DataSource();
  t.truthy(ds);
});
test("initialize()", async t => {
  const ds = new t.context.DataSource();
  ds.initialize({ context: {} });
  t.truthy(ds);
});
