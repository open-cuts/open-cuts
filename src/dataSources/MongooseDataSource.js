/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DataSource } from "./DataSource.js";
import DataLoader from "dataloader";

/**
 * Turns any object into a safe key string
 *
 * @param {Object} obj - object to convert
 * @param {String} [prefix] - prefix string
 * @returns {String} safe key string
 */
const objToKey = (obj, prefix = "") => prefix + "/" + JSON.stringify(obj);

/**
 * Apollo data source for the Mongoose MongoDB client, featuring built-in per-resolve caching, deduplication, memoization and batching. Supports CRUDE operations, complex find queries, and even full-blown aggregation pipelines. This thing has the kitchen sink!
 *
 * Note: Since mongoose has quite advanced batching already built-in, DataLoader's input is not separately batched
 *
 * @private @property {Object} model - mongoose model, do not use directly
 * @property {Object} pipeline - object holding functions corresponding to pipeline operations
 */
export class MongooseDataSource extends DataSource {
  /**
   * @constructs NewMongooseDataSource
   * @param {Object} model - mongoose model
   * @param {Object} pipelines - object of functions returning mongodb aggregation pipelines
   */
  constructor(model, pipelines) {
    super();
    this.model = model;
    this.rawPipelines = pipelines;
    // assign aggregation pipelines as public methods to the pipelines object
    this.pipelines = {};
    for (const pipeline in pipelines) {
      if (pipelines.hasOwnProperty(pipeline)) {
        this.pipelines[pipeline] = (...args) =>
          this.loader.load(["aggregate", pipeline, ...args]);
      }
    }

    this.loader = new DataLoader(
      keys =>
        Promise.all(
          keys.map(([operation, ...args]) => {
            // if a known aggregation pipeline is called, assemble and run it
            if (operation === "aggregate" && pipelines[args[0]])
              return this.model.aggregate(
                this.rawPipelines[args[0]](...args.slice(1))
              );
            else return this.model[operation](...args);
          })
        ),
      {
        cacheKeyFn: objToKey
      }
    );

    // for non-idempotent database operations
    this.uncachedLoader = new DataLoader(
      keys =>
        Promise.all(
          keys.map(([operation, ...args]) => this.model[operation](...args))
        ),
      { cache: false }
    );
  }

  /**
   * Store a document in cache. Only needs to be called if a document was modified outside the datasource.
   *
   * @param {Object} document - document to cache
   */
  cacheDocument(document) {
    if (!document) throw new Error(`${document} is not a document`);
    return this.loader
      .clear(["findById", document._id || document.id])
      .prime(["findById", document._id || document.id], document);
  }

  //////////////////////////////
  // *** crude operations *** //
  //////////////////////////////

  /**
   * Create a document
   *
   * @param {Object} document - mongoose document to create
   */
  create(document) {
    return this.uncachedLoader.load(["create", document]).then(document => {
      this.cacheDocument(document);
      return document;
    });
  }

  /**
   * Find a document by id
   *
   * @param {String} objectID - mongoose object id
   */
  read(objectID) {
    return this.findById(objectID);
  }

  /**
   * Update a document by id. Beware of noSQL injections! As long as GraphQL
   * imput types are enforced, everything should be peachy.
   *
   * @param {String} objectID - mongoose object id
   * @param {Object} updateObject - mongodb update object
   */
  update(objectID, updateObject) {
    return this.uncachedLoader
      .load(["findByIdAndUpdate", objectID, updateObject, { new: true }])
      .then(document => {
        this.cacheDocument(document);
        return document;
      });
  }

  /**
   * Delete a document by id
   *
   * @param {String} objectID - mongoose object id
   */
  delete(objectID) {
    return this.uncachedLoader
      .load(["findByIdAndDelete", objectID])
      .then(document => {
        this.cacheDocument(document);
        return document;
      });
  }

  /**
   * Resolve all documents from the collection
   */
  explore() {
    return this.uncachedLoader.load(["find"]).then(documents => {
      if (Array.isArray(documents))
        documents.forEach(document => this.cacheDocument(document));
      return documents;
    });
  }

  ////////////////////////////////
  // *** mongoose model api *** //
  ////////////////////////////////

  /**
   * Resolves number of documents matching filter in a database collection
   * @param {Object} [filter] - filter for <model>.countDocuments()
   */
  countDocuments(filter) {
    return this.loader.load(["countDocuments", filter]);
  }

  /**
   * Resolve how many matching such document exist
   * @param {...any} args - more args for <model>.distinct()
   */
  distinct(...args) {
    return this.loader.load(["distinct", ...args]);
  }

  /**
   * Resolve true if at least one such document exists
   * @param {...any} args - more args for <model>.exists()
   */
  exists(...args) {
    return this.loader.load(["exists", ...args]);
  }

  /**
   * Find one document by id
   * @param {String} id - mongodb id
   * @param {...any} args - more args for <model>.findById()
   */
  findById(id, ...args) {
    return this.loader.load(["findById", id, ...args]);
  }

  /**
   * Find one document by query object.
   * @param {Object} query - mongoose query
   * @param {...any} args - more args for <model>.findOne()
   */
  findOne(query, ...args) {
    return this.loader.load(["findOne", query, ...args]);
  }

  /**
   * Find documents by query
   * @param {Object} query - mongoose query
   * @param {...any} args - more args for <model>.find()
   */
  find(query, ...args) {
    return this.loader.load(["find", query, ...args]);
  }

  /**
   * Find one document by query object.
   * @param {Object} query - mongoose query
   * @param {Object} mutation - mongoose mutation
   * @param {...any} args - more args for <model>.findOne()
   */
  findOneAndUpdate(query, mutation, ...args) {
    return this.uncachedLoader
      .load(["findOneAndUpdate", query, mutation, ...args])
      .then(document => {
        if (document) this.cacheDocument(document);
        return document;
      });
  }
}
