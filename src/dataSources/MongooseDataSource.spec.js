import test from "ava";
import * as td from "testdouble";

test.beforeEach(async t =>
  import("./MongooseDataSource.js").then(
    ({ MongooseDataSource }) =>
      (t.context.MongooseDataSource = MongooseDataSource)
  )
);

test("exports {MongooseDataSource}", async t => {
  t.truthy(t.context.MongooseDataSource);
});
test("constructor()", async t => {
  const model = td.object({});
  const pipelines = td.object({
    testPipeline: [
      {
        a: "b"
      }
    ]
  });
  const ds = new t.context.MongooseDataSource(model, pipelines);
  t.truthy(ds);
});
