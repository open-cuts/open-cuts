/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { MongooseDataSource } from "./MongooseDataSource.js";
import { SystemModel } from "../models/system.js";

import {
  buildStatus,
  combinationMatrixForBuild,
  testMatrixForBuild
} from "../aggregation/systems.js";

import { parseOcfl } from "../aggregation/ocfl.js";

/**
 * Apollo data source for the test model. Provides CRUDE operations and aggregations.
 */
export class SystemsDataSource extends MongooseDataSource {
  /**
   * @constructs SystemsDataSource
   */
  constructor() {
    super(SystemModel, {
      buildStatus,
      combinationMatrixForBuild,
      testMatrixForBuild
    });
  }

  /**
   * Return channels for a system
   * @param {String} id - mongoose object id
   */
  channels(id) {
    return this.context.dataSources.builds
      .distinct("channel", { system: id })
      .then(channels =>
        Promise.all(
          channels.map(channel =>
            this.context.dataSources.builds
              .byChannel(id, channel)
              .then(builds => ({ channel, builds }))
          )
        )
      );
  }

  /**
   * Return builds for a system
   * @param {String} id - mongoose object id
   */
  builds(id) {
    return this.context.dataSources.builds.find({ system: id });
  }

  /**
   * Return latest build for a system
   * @param {String} id - mongoose object id
   */
  latestBuild(id) {
    return this.context.dataSources.builds.findOne(
      { system: id },
      {},
      { sort: { date: -1 } }
    );
  }

  /**
   * Return tests for a system
   * @param {String} id - mongoose object id
   */
  tests(id) {
    return this.context.dataSources.tests.find({ system: id });
  }

  /**
   * Return tests for a system
   * @param {String} id - mongoose object id
   */
  groups(id) {
    return this.context.dataSources.tests
      .distinct("group", { system: id })
      .then(groups =>
        Promise.all(
          groups.map(group =>
            this.context.dataSources.tests
              .byGroup(id, group)
              .then(tests => ({ group, tests }))
          )
        )
      );
  }

  /**
   * Add property to a system
   * @param {String} systemId - mongoose object id
   * @param {Object} property - new property
   */
  createProperty(systemId, property) {
    return this.update(systemId, {
      $push: { properties: property }
    });
  }

  /**
   * Update property on a system
   * @param {String} propertyId - mongoose object id
   * @param {Object} property - property update
   */
  updateProperty(propertyId, property) {
    return this.findOneAndUpdate(
      { "properties._id": propertyId },
      {
        $set: {
          "properties.$": property
        }
      },
      { new: true }
    );
  }

  /**
   * Delete property from a system
   * @param {String} propertyId - mongoose object id
   */
  deleteProperty(propertyId) {
    return this.findOneAndUpdate(
      { "properties._id": propertyId },
      {
        $pull: {
          properties: { _id: propertyId }
        }
      },
      { new: true }
    );
  }

  /**
   * Set combination filter
   * @param {String} propertyId - mongoose object id
   */
  setCombinationFilter(id, combinationFilter) {
    try {
      parseOcfl(JSON.parse(combinationFilter));
    } catch (error) {
      throw new Error("Invalid OCFL statement");
    }
    return this.update({ _id: id }, { combinationFilter }, { new: true });
  }

  /**
   * Get combination filter
   * @param {String} propertyId - mongoose object id
   */
  getCombinationFilter(id) {
    return this.read(id).then(({ combinationFilter }) =>
      combinationFilter ? JSON.parse(combinationFilter) : {}
    );
  }
}
