/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { MongooseDataSource } from "./MongooseDataSource.js";
import { BuildModel } from "../models/build.js";
import { cache } from "../lib/cache.js";

const combinationToOcfl = combination => ({
  AND: combination.map(({ variable, value }) => ({
    CHECK: [variable, [value]]
  }))
});

/**
 * Apollo data source for the build model. Provides CRUDE operations and aggregations.
 */
export class BuildsDataSource extends MongooseDataSource {
  /**
   * @constructs BuildDataSource
   */
  constructor() {
    super(BuildModel);
  }

  /**
   * get builds for a single channel
   * @param {String} systemId system id
   * @param {String} channel channel name
   * TODO pagination
   */
  byChannel(systemId, channel) {
    return cache.get("byChannel", { systemId, channel }, () =>
      this.find(
        { system: systemId, channel },
        { id: true },
        {
          sort: { date: -1, tag: -1 },
          limit: 6
        }
      ).then(r => r.map(({ id }) => ({ id })))
    );
  }

  /**
   * resolve next id
   * @param {String} buildId - mongoose object id
   * @returns {Promise} - resolves next build document
   */
  next(buildId) {
    return cache.get("next", { buildId }, () =>
      this.read(buildId).then(({ date, tag, system, channel }) =>
        this.find(
          {
            system,
            channel,
            $or: [{ date: { $gt: date } }, { tag: { $gt: tag } }]
          },
          { id: true },
          {
            sort: { date: 1, tag: 1 },
            limit: 1
          }
        ).then(r => (r[0] ? { id: r[0]?.id } : null))
      )
    );
  }

  /**
   * resolve prev id
   * @param {String} buildId - mongoose object id
   * @returns {Promise} - resolves prev build document
   */
  prev(buildId) {
    return cache.get("prev", { buildId }, () =>
      this.read(buildId).then(({ date, tag, system, channel }) =>
        this.find(
          {
            system,
            channel,
            $or: [{ date: { $lt: date } }, { tag: { $lt: tag } }]
          },
          { id: true },
          {
            sort: { date: -1, tag: -1 },
            limit: 1
          }
        ).then(r => (r[0] ? { id: r[0]?.id } : null))
      )
    );
  }

  /**
   * Resolve chart for all builds on the same channel
   * @param {String} buildId - mongoose object id for build
   * @param {String} [testId] - mongoose object id for test
   * @param {Object} [combination] - Combination
   * @returns {Promise} - resolves chart data
   */
  async chart(buildId, testId, combination) {
    const { channel, system, tag } = await this.read(buildId);
    return this.byChannel(system.toString(), channel)
      .then(builds =>
        Promise.all(
          builds.reverse().map(async build => ({
            status: await this.status(build.id, testId, combination),
            tag: (await this.read(build.id)).tag
          }))
        )
      )
      .then(builds => ({
        labels: builds.map(build =>
          build.tag === tag ? `[ ${tag} ]` : build.tag
        ),
        datasets: [
          {
            label: "pass",
            backgroundColor: "green",
            data: builds.map(({ status }) => status.pass)
          },
          {
            label: "wonky",
            backgroundColor: "yellow",
            data: builds.map(({ status }) => status.wonky)
          },
          {
            label: "fail",
            backgroundColor: "red",
            data: builds.map(({ status }) => status.fail)
          },
          {
            label: "inconclusive",
            backgroundColor: "grey",
            data: builds.map(({ status }) => status.inconclusive)
          }
        ]
      }));
  }

  /**
   * Run combinationStatus pipeline
   * @param {String} buildId - mongoose object id
   * @returns {Promise} - resolves combinationStatus
   */
  combinations(buildId) {
    return this.read(buildId).then(build =>
      cache.get("combinations", { systemId: build.system, buildId }, async () =>
        this.context.dataSources.systems.pipelines.combinationMatrixForBuild(
          build.system,
          build.id,
          await this.context.dataSources.systems.getCombinationFilter(
            build.system
          )
        )
      )
    );
  }

  /**
   * Run testStatus pipeline and calculate highlights
   * @param {String} buildId - mongoose object id
   * @returns {Promise} - resolves testStatus
   */
  tests(buildId) {
    return this.read(buildId).then(async build =>
      cache
        .get(
          "testMatrixForBuild",
          { systemId: build.system, buildId },
          async () =>
            this.context.dataSources.systems.pipelines.testMatrixForBuild(
              build.system,
              build.id,
              await this.context.dataSources.systems.getCombinationFilter(
                build.system
              )
            )
        )
        .then(tests =>
          Promise.all(
            tests.map(({ test, status }) =>
              this.context?.user?.id && status.inconclusive > 0
                ? this.context.dataSources.runs
                    .exists({
                      test: test.id,
                      user: this.context?.user?.id,
                      build: buildId
                    })
                    .then(r => !r)
                    .then(highlight => ({ test, status, highlight }))
                : Promise.resolve({ test, status, highlight: false })
            )
          )
        )
    );
  }

  /**
   * Determine whether the build should be highlighted
   * @param {String} objectId - mongoose object id
   * @returns {Promise} - resolves highlight boolean
   */
  highlight(buildId) {
    return this.tests(buildId).then(tests =>
      tests.reduce((acc, curr) => acc || curr.highlight, false)
    );
  }

  /**
   * get status, optionally limiting to a single test or combination
   * @param {String} objectId - mongoose object id
   * @param {String} [testId] - mongoose object id for test
   * @param {Object} [combination] - combination
   * @returns {Promise} - resolves status
   */
  status(objectId, testId, combination) {
    return this.read(objectId).then(async build =>
      cache.get(
        "status",
        { systemId: build.system, buildId: build.id, testId, combination },
        async () =>
          this.context.dataSources.systems.pipelines
            .buildStatus(
              build.system,
              build.id,
              testId,
              combination
                ? combinationToOcfl(combination)
                : await this.context.dataSources.systems.getCombinationFilter(
                    build.system
                  )
            )
            .then(status => status[0])
      )
    );
  }

  /**
   * Run status pipeline and compare with previous result
   * @param {String} objectId - mongoose object id
   * @returns {Promise} - resolves status
   */
  summary(objectId) {
    return Promise.all([
      this.status(objectId),
      this.prev(objectId)
        .then(({ id }) => this.status(id))
        .catch(() => null)
    ])
      .then(([currStatus, prevStatus]) => {
        // if this build is completely inconclusive, we do not need to bother
        if (currStatus.inconclusive === 1) return "INCONCLUSIVE";
        // check if we even have previous information
        const currFailed = currStatus.fail + currStatus.wonky > 0;
        const currConclusive = currStatus.inconclusive === 0;
        if (prevStatus) {
          const prevFailed = prevStatus.fail + prevStatus.wonky > 0;
          if (prevFailed && currFailed) return "STILL_FAILING";
          if (!prevFailed && !currFailed && currConclusive) return "PASS";
          if (prevFailed && !currFailed && currConclusive) return "FIXED";
          if (!prevFailed && currFailed) return "REGRESSION";
        } else {
          if (currFailed) return "STILL_FAILING";
          if (!currFailed && currConclusive) return "PASS";
        }
        return "INCONCLUSIVE"; // TODO optimize boolean logic
      })
      .catch(() => "INCONCLUSIVE");
  }

  /**
   * Return runs for a build
   * @param {String} id - mongoose object id
   * @param {String|Date} cursor - pagination: resolve runs older than cursor
   * @param {Number} limit - pagination: resolve this amount of runs
   */
  runs(id, cursor, limit = 50) {
    return this.context.dataSources.runs.find(
      {
        build: id,
        spam: { $ne: true },
        ...(cursor ? { date: { $lt: cursor } } : {})
      },
      { id: true, date: true },
      { limit: limit || 50, sort: { date: -1 } }
    );
  }

  /**
   * Return runs for a build
   * @param {String} id - mongoose object id
   * @param {String|Date} cursor - pagination: resolve runs older than cursor
   * @param {Number} limit - pagination: resolve this amount of runs
   */
  runFeed(id, cursor, limit = 50) {
    return Promise.all([
      this.runs(id, cursor, limit),
      this.context.dataSources.runs.countDocuments({
        build: id,
        spam: { $ne: true }
      })
    ]).then(async ([runs, count]) => ({
      runs,
      count,
      cursor:
        runs.length &&
        this.context.dataSources.runs
          .exists({
            build: id,
            spam: { $ne: true },
            date: { $lt: runs[runs.length - 1]?.date }
          })
          .then(more => more && runs[runs.length - 1]?.date)
    }));
  }
}
