/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import apolloDataSource from "apollo-datasource";

/**
 * Clean slate for an apollo data source
 * @virtual
 */
export class DataSource extends apolloDataSource.DataSource {
  /**
   * Set shared context
   * inherited from apollos DataSource
   * @private
   * @param {Object} param0 - context
   */
  initialize({ context }) {
    this.context = context;
  }
}
