/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { MongooseDataSource } from "./MongooseDataSource.js";
import { TestModel } from "../models/test.js";

/**
 * Apollo data source for the test model. Provides CRUDE operations and aggregations.
 */
export class TestsDataSource extends MongooseDataSource {
  /**
   * @constructs TestsDataSource
   */
  constructor() {
    super(TestModel);
  }

  /**
   * get tests from one group for a system
   * @param {String} system system id
   * @param {String} group test group name
   */
  byGroup(system, group) {
    return this.find({ system, group }, null, {
      sort: { weight: -1 }
    });
  }

  /**
   * Return runs for a test
   * @param {String} id - mongoose object id
   */
  runs(id) {
    return this.context.dataSources.runs.find({
      test: id,
      spam: { $ne: true }
    });
  }
}
