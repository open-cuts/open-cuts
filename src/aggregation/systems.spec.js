import test from "ava";
import * as td from "testdouble";

const fakeId = "000000000000000000000000";

test.beforeEach(async t =>
  td
    .replaceEsm("./ocfl.js")
    .then(() => import("./systems.js"))
    .then(systems => (t.context = systems))
);

test("exports {systemCombinations}", async t => {
  t.truthy(t.context.systemCombinations);
});
test("systemCombinations() without filter", async t => {
  const pipeline = t.context.systemCombinations(fakeId);
  t.is(pipeline.length, 4);
});

test("exports {fullMatrix}", async t => {
  t.truthy(t.context.fullMatrix);
});
test("fullMatrix() without filter", async t => {
  const pipeline = t.context.fullMatrix(fakeId, fakeId, fakeId);
  t.is(pipeline.length, 12);
});
test("fullMatrix() without filter and buildId", async t => {
  const pipeline = t.context.fullMatrix(fakeId, null, fakeId);
  t.is(pipeline.length, 12);
});

test("exports {testCombinationMatrixForBuild}", async t => {
  t.truthy(t.context.testCombinationMatrixForBuild);
});
test("testCombinationMatrixForBuild() without filter", async t => {
  const pipeline = t.context.testCombinationMatrixForBuild(fakeId, fakeId);
  t.is(pipeline.length, 13);
});

test("exports {testMatrixForBuild}", async t => {
  t.truthy(t.context.testMatrixForBuild);
});
test("testMatrixForBuild() without filter", async t => {
  const pipeline = t.context.testMatrixForBuild(fakeId, fakeId);
  t.is(pipeline.length, 15);
});

test("exports {combinationMatrixForBuild}", async t => {
  t.truthy(t.context.combinationMatrixForBuild);
});
test("combinationMatrixForBuild() without filter", async t => {
  const pipeline = t.context.combinationMatrixForBuild(fakeId, fakeId);
  t.is(pipeline.length, 15);
});

test("exports {buildStatus}", async t => {
  t.truthy(t.context.buildStatus);
});
test("buildStatus() without filter", async t => {
  const pipeline = t.context.buildStatus(fakeId, fakeId);
  t.is(pipeline.length, 14);
});
