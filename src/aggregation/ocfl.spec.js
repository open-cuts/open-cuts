import test from "ava";

const fakeId = "000000000000000000000000";

test.beforeEach(async t =>
  import("./ocfl.js").then(({ parseOcfl }) => (t.context = { parseOcfl }))
);

test("exports {parseOcfl}", async t => {
  t.truthy(t.context.parseOcfl);
});

[
  "nope",
  { WHATTHEFUCK: {} },
  { AND: {}, OR: {} },
  { CHECK: { wasd: {} } },
  { AND: { wasd: {} } },
  { OR: { wasd: {} } },
  { AND: [] },
  { OR: [] },
  { NOT: [] },
  { IMPLIES: [] }
].forEach(input =>
  test(`should throw error for ${JSON.stringify(input)}`, async t => {
    t.throws(() => t.context.parseOcfl(input), {
      instanceOf: Error,
      message: `OCFL Error: Invalid expression ${JSON.stringify(input)}`
    });
  })
);

[undefined, null, {}].forEach(input =>
  test(`should return falsy for ${input}`, async t => {
    const parsed = t.context.parseOcfl(input);
    t.true(parsed);
  })
);
test(`should accept valid simple expression`, async t => {
  const parsed = t.context.parseOcfl({
    CHECK: ["Boolean", ["true"]]
  });
  t.deepEqual(parsed, {
    $or: [
      {
        $in: [
          {
            value: "true",
            variable: "Boolean"
          },
          "$$c"
        ]
      }
    ]
  });
});
test(`should accept valid complex expression`, async t => {
  const parsed = t.context.parseOcfl({
    OR: [
      { CHECK: ["Boolean", ["true"]] },
      {
        AND: [
          {
            IMPLIES: [
              { CHECK: ["Boolean", ["false"]] },
              { CHECK: ["List", ["a", "b"]] }
            ]
          }
        ]
      },
      {
        NOT: {
          CHECK: ["Boolean", ["false"]]
        }
      }
    ]
  });
  t.deepEqual(parsed, {
    $or: [
      {
        $or: [
          {
            $in: [
              {
                variable: "Boolean",
                value: "true"
              },
              "$$c"
            ]
          }
        ]
      },
      {
        $and: [
          {
            $or: [
              {
                $not: [
                  {
                    $or: [
                      {
                        $in: [
                          {
                            variable: "Boolean",
                            value: "false"
                          },
                          "$$c"
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                $or: [
                  {
                    $in: [
                      {
                        variable: "List",
                        value: "a"
                      },
                      "$$c"
                    ]
                  },
                  {
                    $in: [
                      {
                        variable: "List",
                        value: "b"
                      },
                      "$$c"
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        $not: [
          {
            $or: [
              {
                $in: [
                  {
                    variable: "Boolean",
                    value: "false"
                  },
                  "$$c"
                ]
              }
            ]
          }
        ]
      }
    ]
  });
});
