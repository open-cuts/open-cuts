/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Builds a MongoDB aggregation expression from an OCFL statement
 *
 * OCFL specification
 *
 * The OPENCUTS Combination Filtering Language is a simple logical language
 * for creating boolean expressions that are compiled into MongoDB Aggregation
 * pipelines. The Language is a subset of JSON.
 *
 * exp       : <check> | <unaryexp> | <binaryexp> | <chainexp>
 * check     : { CHECK: [<stringterminal>, [<stringterminal>, ...]] }
 * unaryexp  : { <unaryop>: [<exp>] }
 * unaryop   : NOT
 * binaryexp : { <binaryop>: [<exp>, <exp>] }
 * binaryop  : IMPLIES
 * chainexp  : { <chainop>: [<exp>, ...] }
 * chainop   : AND | OR
 *
 * TODO add support for more operators
 *
 * @param {Object} ocflExpression - OCFL expression
 * @returns {Object} - MongoDB aggregation expression object
 */
export function parseOcfl(ocflExpression) {
  try {
    if (!ocflExpression) return true;
    const expr = Object.entries(ocflExpression);
    if (!expr || expr.length === 0) return true;

    if (expr.length !== 1) {
      throw new Error(
        `OCFL Error: Invalid expression ${JSON.stringify(ocflExpression)}`
      );
    }

    switch (expr[0][0]) {
      case "CHECK":
        expr[0][0] = "$or";
        expr[0][1] = expr[0][1][1].map(value => ({
          $in: [
            {
              variable: expr[0][1][0],
              value
            },
            "$$c"
          ]
        }));
        break;
      case "AND":
        expr[0][0] = "$and";
        ensureChain("AND", expr[0][1]);
        expr[0][1] = expr[0][1].map(parseOcfl);
        break;
      case "OR":
        expr[0][0] = "$or";
        ensureChain("OR", expr[0][1]);
        expr[0][1] = expr[0][1].map(parseOcfl);
        break;
      case "NOT":
        expr[0][0] = "$not";
        ensureUnary("NOT", expr[0][1]);
        expr[0][1] = [parseOcfl(expr[0][1])];
        break;
      case "IMPLIES":
        expr[0][0] = "$or";
        ensureBinary("IMPLIES", expr[0][1]);
        // (a => b) <=> ((not a) or b)
        expr[0][1] = [
          {
            $not: [parseOcfl(expr[0][1][0])]
          },
          parseOcfl(expr[0][1][1])
        ];
        break;
      default:
        throw new Error(`OCFL Error: Unrecognized operator ${expr[0][0]}`);
    }

    return Object.fromEntries(expr);
  } catch (error) {
    throw new Error(
      `OCFL Error: Invalid expression ${JSON.stringify(ocflExpression)}`
    );
  }
}

function ensureUnary(operator, args) {
  if (Array.isArray(args))
    throw new Error(`OCFL Error: ${operator} is an unary operator`);
}

function ensureBinary(operator, args) {
  if (args.length !== 2)
    throw new Error(`OCFL Error: ${operator} is a binary operator`);
}

function ensureChain(operator, args) {
  if (args.length < 1)
    throw new Error(`OCFL Error: ${operator} is a chain operator`);
}
