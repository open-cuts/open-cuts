/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import mongoose from "mongoose";
import { parseOcfl } from "./ocfl.js";
const { Types } = mongoose;

const REQUIRED_RUNS = 2;

/**
 * Returns a MongoDB aggregation pipeline to calculate the possible
 * combinations (i.e. cartesian product of all variation values).
 *
 * Intended to be used as the beginning of the pipeline on "systems"
 * and results in an object like this:
 * [{
 *   combinations: [
 *     [
 *       {variable: "Variation name", value: "Variation value"}, ...
 *     ]
 *   ]
 * }]
 *
 * @param {String} systemId - System ID
 * @param {Object} [filter] - OCFL filtering statement
 * @returns {Array} - Array of MongoDB Aggregation pipeline steps
 */
export const systemCombinations = (systemId, filter = {}) => [
  // Find System
  { $match: { _id: Types.ObjectId(systemId) } },
  // Build two-dimensional array of {variable, value} objects
  {
    $project: {
      properties: {
        $map: {
          input: "$properties",
          as: "prop",
          in: {
            $map: {
              input: "$$prop.values",
              as: "v",
              in: { variable: "$$prop.name", value: "$$v" }
            }
          }
        }
      }
    }
  },
  // Calculate combinations
  {
    $project: {
      combinations: {
        $cond: {
          if: {
            $and: [
              { $isArray: "$properties" },
              { $eq: [1, { $size: "$properties" }] }
            ]
          },
          then: {
            // If there is only one axis, the properties constitute the combinations
            $map: {
              input: { $arrayElemAt: ["$properties", 0] },
              as: "prop",
              in: ["$$prop"]
            }
          },
          else: {
            // If there are multiple axes, calculate the cartesian product
            $reduce: {
              input: {
                $slice: [
                  "$properties",
                  1,
                  { $max: [1, { $subtract: [{ $size: "$properties" }, 1] }] }
                ]
              },
              initialValue: { $arrayElemAt: ["$properties", 0] },
              in: {
                $let: {
                  vars: {
                    currentr: "$$this",
                    currenta: "$$value"
                  },
                  in: {
                    $reduce: {
                      input: {
                        $map: {
                          input: "$$currenta",
                          as: "a",
                          in: {
                            $map: {
                              input: "$$currentr",
                              as: "r",
                              in: {
                                $concatArrays: [
                                  {
                                    $cond: {
                                      if: { $isArray: "$$a" },
                                      then: "$$a",
                                      else: ["$$a"]
                                    }
                                  },
                                  ["$$r"]
                                ]
                              }
                            }
                          }
                        }
                      },
                      initialValue: [],
                      in: {
                        $concatArrays: ["$$value", "$$this"]
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  {
    $project: {
      combinations: {
        $filter: {
          input: "$combinations",
          as: "c",
          cond: parseOcfl(filter)
        }
      }
    }
  }
];

/**
 * Returns a MongoDB aggregation pipeline to calculate the status
 * for all possible combinations and accumulate the runs for each
 * of their builds and runs.
 *
 * Intended to be used as the beginning of the pipeline on "systems"
 * and results in an array of objects like this:
 * {
 *   test: { id },
 *   build: { id },
 *   combination: [{variable: "Variation", value: "value"}, ...]
 *   status: { pass, wonky, fail, inconclusive }
 * }
 *
 * @param {String} systemId - System ID
 * @param {String} [buildId] - specify Build ID if you want to filter
 * @param {String} [testId] - specify Test ID if you want to filter
 * @param {Object} [filter] - OCFL filtering statement
 * @returns {Array} - Array of MongoDB Aggregation pipeline steps
 */
export const fullMatrix = (systemId, buildId, testId, filter = {}) => [
  ...systemCombinations(systemId, filter),
  // look up builds
  {
    $lookup: {
      from: "builds",
      let: {
        mSystem: "$_id"
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                buildId
                  ? { $eq: ["$_id", Types.ObjectId(buildId)] }
                  : { $eq: ["$system", "$$mSystem"] }
              ]
            }
          }
        },
        { $sort: { tag: -1 } }
      ],
      as: "builds"
    }
  },
  // look up tests
  {
    $lookup: {
      from: "tests",
      let: {
        mSystem: "$_id"
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                testId
                  ? { $eq: ["$_id", Types.ObjectId(testId)] }
                  : { $eq: ["$system", "$$mSystem"] }
              ]
            }
          }
        },
        { $sort: { name: -1 } }
      ],
      as: "tests"
    }
  },
  // project to structure for unwinding
  {
    $project: {
      _id: 0,
      combination: { $concatArrays: [[], "$combinations"] },
      test: {
        $map: {
          input: "$tests",
          as: "t",
          in: "$$t._id"
        }
      },
      build: {
        $map: {
          input: "$builds",
          as: "b",
          in: "$$b._id"
        }
      }
    }
  },
  // unwind that motherfucker
  { $unwind: "$test" },
  { $unwind: "$build" },
  {
    $unwind: {
      path: "$combination",
      preserveNullAndEmptyArrays: true
    }
  },
  // look up runs and calculate status
  {
    $lookup: {
      from: "runs",
      let: {
        mTest: "$test",
        mBuild: "$build",
        mCombination: "$combination"
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $and: [
                { $eq: ["$test", "$$mTest"] },
                { $eq: ["$build", "$$mBuild"] }
              ]
            }
          }
        },
        { $project: { "combination._id": 0 } },
        {
          $match: {
            $expr: {
              $cond: {
                if: "$$mCombination",
                then: { $eq: ["$combination", "$$mCombination"] },
                else: true
              }
            }
          }
        },
        ...groupRuns
      ],
      as: "status"
    }
  },
  // project to a human-readable structure and prevent empty status
  {
    $project: {
      _id: 0,
      status: {
        $arrayElemAt: [
          {
            $cond: {
              if: { $gte: [{ $size: "$status" }, 1] },
              then: "$status.status",
              else: [{ pass: 0, wonky: 0, fail: 0, inconclusive: 1 }]
            }
          },
          0
        ]
      },
      test: { id: "$test" },
      build: { id: "$build" },
      combination: "$combination"
    }
  }
];

/**
 * Returns a MongoDB aggregation pipeline to calculate the status
 * for all possible combinations on all tests and accumulate the
 * runs on a single build.
 *
 * Intended to be used as the beginning of the pipeline on "systems"
 * and results in an array of objects like this:
 * {
 *   test: { id },
 *   combination: [{variable: "Variation", value: "value"}, ...]
 *   status: { pass, wonky, fail, inconclusive }
 * }
 *
 * @param {String} systemId - System ID
 * @param {String} buildId - Build ID
 * @param {Object} [filter] - OCFL filtering statement
 * @returns {Array} - Array of MongoDB Aggregation pipeline steps
 */
export const testCombinationMatrixForBuild = (
  systemId,
  buildId,
  filter = {}
) => [
  ...fullMatrix(systemId, buildId, null, filter),
  {
    $project: {
      _id: 0,
      test: "$test",
      combination: "$combination",
      status: "$status"
    }
  }
];

/**
 * Returns a MongoDB aggregation pipeline to calculate the status
 * for all possible combinations on all tests and accumulate the
 * runs on a single build.
 *
 * Intended to be used as the beginning of the pipeline on "systems"
 * and results in an array of objects like this:
 * {
 *   test: { id },
 *   status: { pass, wonky, fail, inconclusive }
 * }
 *
 * @param {String} systemId - System ID
 * @param {String} buildId - Build ID
 * @param {Object} [filter] - OCFL filtering statement
 * @returns {Array} - Array of MongoDB Aggregation pipeline steps
 */
export const testMatrixForBuild = (systemId, buildId, filter = {}) => [
  ...testCombinationMatrixForBuild(systemId, buildId, filter),
  {
    $group: {
      _id: { test: "$test" },
      ...statusGrouper
    }
  },
  {
    $project: {
      _id: 0,
      test: "$_id.test",
      status: statusProjector
    }
  }
];

/**
 * Returns a MongoDB aggregation pipeline to calculate the status
 * for all possible combinations and accumulate the runs on a
 * single build.
 *
 * Intended to be used as the beginning of the pipeline on "systems"
 * and results in an array of objects like this:
 * {
 *   combination: [{variable: "Variation", value: "value"}, ...]
 *   status: { pass, wonky, fail, inconclusive }
 * }
 *
 * @param {String} systemId - System ID
 * @param {String} buildId - Build ID
 * @param {Object} [filter] - OCFL filtering statement
 * @returns {Array} - Array of MongoDB Aggregation pipeline steps
 */
export const combinationMatrixForBuild = (systemId, buildId, filter = {}) => [
  ...testCombinationMatrixForBuild(systemId, buildId, filter),
  {
    $group: {
      _id: "$combination",
      ...statusGrouper
    }
  },
  {
    $project: {
      _id: 0,
      combination: "$_id",
      status: statusProjector
    }
  }
];

/**
 * Returns a MongoDB aggregation pipeline to calculate the overall
 * build status
 *
 * Intended to be used as the beginning of the pipeline on "systems"
 * and results in an object like this:
 * { pass, wonky, fail, inconclusive }
 *
 * @param {String} systemId - System ID
 * @param {String} buildId - Build ID
 * @param {String} [testId] - specify Test ID if you want to filter
 * @param {Object} [filter] - OCFL filtering statement
 * @returns {Array} - Array of MongoDB Aggregation pipeline steps
 */
export const buildStatus = (systemId, buildId, testId, filter = {}) => [
  ...fullMatrix(systemId, buildId, testId, filter),
  {
    $group: {
      _id: null,
      ...statusGrouper
    }
  },
  {
    $project: {
      _id: 0,
      ...statusProjector
    }
  }
];

const statusPercentageAccumulator = group => ({
  $divide: [`$${group}`, "$count"]
});

/**
 * Helper for summing up status objects, needs re-projecting afterwards
 */
const statusGrouper = {
  pass: { $sum: "$status.pass" },
  wonky: { $sum: "$status.wonky" },
  fail: { $sum: "$status.fail" },
  inconclusive: { $sum: "$status.inconclusive" },
  count: { $sum: 1 }
};

/**
 * Helper for re-projecting grouped status
 */
const statusProjector = {
  pass: statusPercentageAccumulator("pass"),
  wonky: statusPercentageAccumulator("wonky"),
  fail: statusPercentageAccumulator("fail"),
  inconclusive: statusPercentageAccumulator("inconclusive")
};

/**
 * MongoDB aggregation pipeline to group runs by results and
 * calculate totals
 *
 * TODO document this properly and re-structure
 *
 * @param {String} systemId - System ID
 * @returns {Array} - Array of MongoDB Aggregation pipeline steps
 */
const groupRuns = [
  {
    $group: {
      _id: "$result",
      total: { $sum: 1 }
    }
  },
  {
    $group: {
      _id: null,
      status: {
        $push: {
          result: "$_id",
          total: "$total"
        }
      }
    }
  },
  // calculate status in absolute numbers
  {
    $project: {
      _id: 0,
      total: {
        // fill up totals
        $max: [REQUIRED_RUNS, { $sum: "$status.total" }]
      },
      status: {
        $mergeObjects: [
          {
            pass: 0,
            wonky: 0,
            fail: 0,
            inconclusive: {
              // If there's less than ten runs, fill up with 'inconclusive' status
              $max: [
                0,
                { $subtract: [REQUIRED_RUNS, { $sum: "$status.total" }] }
              ]
            }
          },
          {
            $arrayToObject: {
              $map: {
                input: "$status",
                as: "el",
                in: {
                  k: { $toLower: "$$el.result" },
                  v: "$$el.total"
                }
              }
            }
          }
        ]
      }
    }
  },
  // calculate status percentages
  {
    $project: {
      _id: 0,
      status: {
        pass: { $divide: ["$status.pass", "$total"] },
        wonky: { $divide: ["$status.wonky", "$total"] },
        fail: { $divide: ["$status.fail", "$total"] },
        inconclusive: { $divide: ["$status.inconclusive", "$total"] }
      }
    }
  }
];
