/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import express from "express";
import mongoose from "mongoose";
import { rateLimiterMiddleware } from "./lib/rateLimiter.js";
import { ApolloServer } from "apollo-server-express";
import {
  ApolloServerPluginLandingPageGraphQLPlayground,
  ApolloServerPluginLandingPageDisabled
} from "apollo-server-core";
import depthLimit from "graphql-depth-limit";
import nodemailer from "nodemailer";
import { UserModel } from "./models/user.js";
import RunClassifier from "./lib/RunClassifier.js";
import createLogger from "debug";
import dataSources from "./dataSources/_index.js";
import modules from "./graphql/_index.js";
import { housekeeping } from "./lib/housekeeping.js";
import { seedDatabase } from "./lib/seeder.js";

const logger = {
  debug: createLogger("debug"),
  info: createLogger("info"),
  warn: createLogger("warn"),
  error: createLogger("error")
};

const HOUR = 3600000;

export const server = async ({ SMTP_CONFIG, MONGODB_URL, NODE_ENV, PORT }) => {
  global.smtp = SMTP_CONFIG
    ? nodemailer.createTransport(JSON.parse(SMTP_CONFIG))
    : null;

  try {
    await mongoose.connect(MONGODB_URL);
  } catch (err) {
    throw new Error(`MongoError: ${err}`);
  }

  const app = express();
  app.use(rateLimiterMiddleware);

  const server = new ApolloServer({
    ...(await modules()),
    plugins: [
      NODE_ENV === "production"
        ? ApolloServerPluginLandingPageDisabled()
        : ApolloServerPluginLandingPageGraphQLPlayground()
    ],
    context: async ({ req }) => {
      // Add user to context, if a token is provided
      const token = req.headers.authorization || "";
      const user = (await UserModel.findOne({ token })) || null;
      return { user };
    },
    dataSources,
    logger,
    validationRules: [depthLimit(10)]
  });

  await server.start();
  server.applyMiddleware({ app });

  app.get("/", (req, res) => res.redirect(302, "/graphql"));

  const expressServer = app.listen({ port: PORT || 4001 }, () =>
    logger.info("--- open-cuts connected and online ---")
  );
  global.killServer = () => {
    expressServer.close();
    mongoose.disconnect();
    clearInterval(interval);
  };

  seedDatabase().then(() => housekeeping());
  const interval = setInterval(housekeeping, HOUR);

  // FIXME: potential race condition
  global.runClassifier = new RunClassifier();
};
