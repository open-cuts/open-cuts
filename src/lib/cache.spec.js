import test from "ava";
import * as td from "testdouble";
import mongoUnit from "mongo-unit";
import mongoose from "mongoose";
import { CacheModel } from "../models/cache.js";

const fakeId = id =>
  mongoose.Types.ObjectId(
    Array(24 - id.toString().length)
      .fill(0)
      .join("") + id
  );

const TESTDATA = {
  cache: [
    // for testing cache hits
    {
      fn: "cache_hit",
      args: { systemId: fakeId(0).toString() },
      value: { a: "b" },
      ttl: Date.now() + 9999999
    },
    {
      fn: "cache_miss_outdated",
      args: { systemId: fakeId(0).toString() },
      value: { data: "outdated" },
      ttl: Date.now() - 9999999
    },

    // for testing delete()
    { args: { channel: "delete/this" } },
    { args: { channel: "delete/this", systemId: fakeId(0).toString() } },

    // for testing onRunChanged()
    {
      args: {
        systemId: fakeId(1).toString(),
        buildId: fakeId(6).toString(),
        testId: fakeId(0).toString(),
        channel: "on/run/changed",
        combination: [
          {
            variable: "a",
            value: "foo"
          },
          {
            variable: "b",
            value: "bar"
          }
        ]
      }
    },
    {
      args: {
        systemId: fakeId(1).toString(),
        buildId: fakeId(6).toString(),
        channel: "on/run/changed",
        combination: [
          {
            variable: "a",
            value: "foo"
          },
          {
            variable: "b",
            value: "bar"
          }
        ]
      }
    },
    {
      args: {
        systemId: fakeId(1).toString(),
        buildId: fakeId(6).toString(),
        channel: "on/run/changed",
        testId: fakeId(1).toString()
      }
    },
    {
      args: {
        systemId: fakeId(1).toString(),
        buildId: fakeId(6).toString(),
        channel: "on/run/changed",
        testId: fakeId(1).toString(),
        combination: [
          {
            variable: "a",
            value: "foo"
          },
          {
            variable: "b",
            value: "bar"
          }
        ]
      }
    },

    // for onBuildChanged()
    {
      args: {
        systemId: fakeId(2).toString(),
        buildId: fakeId(6).toString(),
        channel: "on/build/deleted"
      }
    },
    {
      args: {
        buildId: fakeId(2).toString(),
        combination: []
      }
    },

    // for onTestChanged
    {
      args: {
        systemId: fakeId(4).toString(),
        testId: fakeId(5).toString()
      }
    },
    {
      args: {
        systemId: fakeId(4).toString(),
        testId: fakeId(4).toString()
      }
    },
    {
      args: {
        systemId: fakeId(4).toString()
      }
    }
  ],
  builds: [
    {
      _id: fakeId(6),
      system: fakeId(1),
      channel: "on/run/changed"
    }
  ]
};

test.before("start mock db", () =>
  mongoUnit
    .start({ version: "4.2.6", platform: "linux", port: 1337 })
    .then(() => mongoUnit.initDb(TESTDATA))
    .then(() => mongoose.connect(mongoUnit.getUrl()))
);
test.after.always("stop mock db", () => mongoUnit.stop());

test.beforeEach(async t =>
  Promise.all([td.replaceEsm("../lib/utils.js")])
    .then(() => import("./cache.js"))
    .then(ex => (t.context.modules = ex))
);

test("get() should hit cache", async t => {
  const r = await t.context.modules.cache.get(
    "cache_hit",
    { systemId: fakeId(0) },
    () => {
      throw new Error("should not be called");
    }
  );
  t.deepEqual(r, { a: "b" });
  t.pass();
});

test("get() should miss cache if not stored", async t => {
  const callback = td.func("callback");
  td.when(callback()).thenResolve({ data: "should create new" });
  const r = await t.context.modules.cache.get(
    "cache_miss_new",
    { systemId: fakeId(0) },
    callback
  );
  t.deepEqual(r, { data: "should create new" });
  const newDocument = await CacheModel.findOne({ fn: "cache_miss_new" });
  t.deepEqual(newDocument.value, { data: "should create new" });
  t.true(newDocument.ttl > Date.now());
});

test("get() should miss cache if outdated", async t => {
  const callback = td.func("callback");
  td.when(callback()).thenResolve({ data: "should overwrite" });
  const r = await t.context.modules.cache.get(
    "cache_miss_outdated",
    { systemId: fakeId(0) },
    callback
  );
  t.deepEqual(r, { data: "should overwrite" });
  const newDocument = await CacheModel.findOne({ fn: "cache_miss_outdated" });
  t.deepEqual(newDocument.value, { data: "should overwrite" });
  t.true(newDocument.ttl > Date.now());
});

test("get() should not store falsy", async t => {
  const callback = td.func("callback");
  td.when(callback()).thenResolve(null);
  const r = await t.context.modules.cache.get(
    "cache_miss_falsy",
    { systemId: fakeId(0) },
    callback
  );
  t.is(r, null);
  const count = await CacheModel.count({ fn: "cache_miss_falsy" });
  t.is(count, 0);
});

test("delete() should delete matching documents", async t => {
  const countBefore = await CacheModel.count({ "args.channel": "delete/this" });
  t.is(countBefore, 2);
  const { deletedCount, acknowledged } = await t.context.modules.cache.delete({
    channel: "delete/this"
  });
  t.true(acknowledged);
  t.is(deletedCount, 2);
  const countAfter = await CacheModel.count({ "args.channel": "delete/this" });
  t.is(countAfter, 0);
});

test("onRunChanged() should delete all documents referencing the same systemId, buildId, channel, testId (or none), or combination (or none)", async t => {
  const { deletedCount, acknowledged } =
    await t.context.modules.cache.onRunChanged({
      _id: fakeId(1),
      test: fakeId(1),
      build: fakeId(6),
      combination: [
        {
          variable: "a",
          value: "foo"
        },
        {
          variable: "b",
          value: "bar"
        }
      ]
    });
  t.true(acknowledged);
  t.is(deletedCount, 3);
});

test("onBuildChanged() should delete all documents referencing either this build or the same channel on the system", async t => {
  const r = await t.context.modules.cache.onBuildChanged({
    _id: fakeId(2),
    channel: "on/build/deleted",
    system: fakeId(2)
  });
  t.true(r[0].acknowledged);
  t.true(r[1].acknowledged);
  t.is(r[0].deletedCount, 1);
  t.is(r[1].deletedCount, 1);
});

test("onTestChanged() should delete all documents referencing either this test or the same system and no test", async t => {
  const { deletedCount, acknowledged } =
    await t.context.modules.cache.onTestChanged({
      _id: fakeId(4),
      system: fakeId(4)
    });
  t.true(acknowledged);
  t.is(deletedCount, 2);
});
