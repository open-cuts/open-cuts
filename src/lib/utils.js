/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import crypto from "crypto";
import { UserModel } from "../models/user.js";

/**
 * Options to pass into mongoose schemas
 */
export const defaultSchemaOptions = {
  toObject: {
    transform: (doc, ret, options) => {
      ret.id = ret._id; // ensure id is set
    }
  }
};

/**
 * Resolves if the calling user context is a member of the group, else rejects an error
 *
 * @param {object} context - GraphQL resolver context
 * @param {UserModel} context.user - User object
 * @param {String} group - Group to ensure
 * @returns {Promise}
 */
export const ensureGroup = (context, group) =>
  context &&
  context.user &&
  context.user.groups &&
  context.user.groups.includes(group)
    ? Promise.resolve()
    : Promise.reject(
        new Error(
          `Insufficient permissions: You need to be a member of the group '${group}' to perform this query or mutation`
        )
      );

/**
 * Resolves if the calling user context matches the id or the user is an ADMIN, else rejects an error
 *
 * @param {object} context - GraphQL resolver context
 * @param {UserModel} context.user - User object
 * @param {String} id - ID to compare
 * @returns {Promise}
 */
export const ensureSelfOrAdmin = (context, id) =>
  context &&
  context.user &&
  context.user.id &&
  context.user.id === id.toString("hex")
    ? Promise.resolve()
    : ensureGroup(context, "ADMIN").catch(() =>
        Promise.reject(
          new Error(
            "Insufficient permissions: You can only perform this operation on data you own."
          )
        )
      );

/**
 * Resolves if the calling user context matches the id or the user is a MOD, else rejects an error
 *
 * @param {object} context - GraphQL resolver context
 * @param {UserModel} context.user - User object
 * @param {String} id - ID to compare
 * @returns {Promise}
 */
export const ensureSelfOrMod = (context, id) =>
  id &&
  context &&
  context.user &&
  context.user.id &&
  context.user.id === id.toString("hex")
    ? Promise.resolve()
    : ensureGroup(context, "MOD").catch(() =>
        Promise.reject(
          new Error(
            "Insufficient permissions: You can only perform this operation on data you own."
          )
        )
      );

/**
 * Resolves if the caller is not logged in, else rejects an error
 *
 * @param {object} context - GraphQL resolver context
 * @param {UserModel} context.user - User object
 * @returns {Promise}
 */
export const ensureGuest = context =>
  !context || !context.user
    ? Promise.resolve()
    : Promise.reject(
        new Error(`You must not be logged in to perform this query or mutation`)
      );

/**
 * Send an email for confirmation
 *
 * @param {UserModel} user - user object
 */
export const sendConfirmationMail = async user => {
  global.smtp.sendMail({
    from: "OPEN-CUTS account service <noreply@open-cuts.org>",
    to: `${user.name} <${user.email}>`,
    subject: "Your OPEN-CUTS registration",
    text: `Hey ${user.name}! An account on ${process.env.OPENCUTS_FRONTEND_URL} was just created with your email address. You can complete your account setup by entering the confirmation code ${user.confirm} here: ${process.env.OPENCUTS_FRONTEND_URL}/confirm\n\nIf this was not you, you do not need to take any further action. We will not bother you again. Have a great day :)`,
    html: `<p>Hey <b>${user.name}</b>! An account on <a href="${process.env.OPENCUTS_FRONTEND_URL}">${process.env.OPENCUTS_FRONTEND_URL}</a> was just created with your email address. You can complete your account setup by entering the confirmation code ${user.confirm} <a href="${process.env.OPENCUTS_FRONTEND_URL}/confirm">here</a>.<br><br><br><br><br><a href="${process.env.OPENCUTS_FRONTEND_URL}/confirm" style="font-size: 5rem; padding: 2rem; background: lightgrey; font-weight: bold;">${user.confirm}</a><br><br><br><br><br>If this was not you, you do not need to take any further action. We will not bother you again.<br>Have a great day :)</p>`
  });
};

/**
 * Send token to a new admin
 *
 * @param {UserModel} user - user object
 */
export const sendWelcomeAdminMail = async user => {
  global.smtp.sendMail({
    from: "OPEN-CUTS account service <noreply@open-cuts.org>",
    to: `${user.name} <${user.email}>`,
    subject: "Your new OPEN-CUTS installation",
    text: `Hey ${user.name}! You're the new admin for the new OPEN-CUTS installation hosted at ${process.env.OPENCUTS_FRONTEND_URL}! Your OPEN-CUTS installation has been initialized with some seeding data to demonstrate the core concepts and features of the open crowdsourced user testing suite. You can complete your account setup by logging in with your token ${user.token} here: ${process.env.OPENCUTS_FRONTEND_URL}/login\n\nBe sure to select a secure password next! If you want to learn more about what OPEN-CUTS can do for your open source community, visit https://docs.open-cuts.org/first-steps Have a great day, and enjoy exploring OPEN-CUTS :)`,
    html: `<p>Hey <b>${user.name}</b>! You're the new admin for the new OPEN-CUTS installation at <a href="${process.env.OPENCUTS_FRONTEND_URL}">${process.env.OPENCUTS_FRONTEND_URL}</a>! Your OPEN-CUTS installation has been initialized with some seeding data to demonstrate the core concepts and features of the open crowdsourced user testing suite. You can complete your account setup by logging in with your token <b>${user.token}</b> <a href="${process.env.OPENCUTS_FRONTEND_URL}/login">here</a>.<br><br>Be sure to select a secure password next! If you want to learn more about what OPEN-CUTS can do for your open source community, visit <a href="https://docs.open-cuts.org/first-steps">docs.open-cuts.org/first-steps</a> <br> Have a great day, and enjoy exploring OPEN-CUTS :)</p>`
  });
};

/**
 * Send token by mail
 *
 * @param {UserModel} user - user object
 */
export const sendTokenMail = async user => {
  global.smtp.sendMail({
    from: "OPEN-CUTS account service <noreply@open-cuts.org>",
    to: `${user.name} <${user.email}>`,
    subject: "Your OPEN-CUTS token",
    text: `Hey ${user.name}! Someone (you?) requested the api token ${user.token} for ${process.env.OPENCUTS_FRONTEND_URL}. You can use it to log in at ${process.env.OPENCUTS_FRONTEND_URL}/login\n\nYou might want to update your token under ${process.env.OPENCUTS_FRONTEND_URL}/account afterwards. Have a great day :)`,
    html: `<p>Hey <b>${user.name}</b>! Someone (you?) requested the api token <b>${user.token}</b> for <a href="${process.env.OPENCUTS_FRONTEND_URL}">${process.env.OPENCUTS_FRONTEND_URL}</a>. You can use it to log in at <a href="${process.env.OPENCUTS_FRONTEND_URL}/login">${process.env.OPENCUTS_FRONTEND_URL}/login</a>.<br><br>You might want to update your token under <a href="${process.env.OPENCUTS_FRONTEND_URL}/account">${process.env.OPENCUTS_FRONTEND_URL}/account</a> afterwards. <br>Have a great day :)</p>`
  });
};

/**
 * Generate confirmation code
 *
 * @returns {String} string consisting of six random digits
 */
export const generateConfirmationCode = () =>
  (Array(6).join("0") + Math.floor(Math.random() * 999999)).slice(-6);

/**
 * Calculate and return a salted password hash
 *
 * @param {String} password - Password
 * @returns {String} salted hash
 */
export const hashPassword = password =>
  crypto
    .createHmac("sha256", process.env.SECRET)
    .update(password)
    .digest("hex");
