/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SystemModel } from "../models/system.js";
import { TestModel } from "../models/test.js";
import { BuildModel } from "../models/build.js";
import { UserModel } from "../models/user.js";
import { RunModel } from "../models/run.js";
import { sendWelcomeAdminMail } from "./utils.js";

const seedSystem = {
  name: "OPEN-CUTS",
  description: "open crowdsourced user testing suite",
  image: "https://www.open-cuts.org/logo.svg",
  properties: [
    {
      name: "NODE_ENV",
      description: "The NODE_ENV environment variable",
      link: "https://stackoverflow.com/questions/16978256/what-is-node-env-and-how-to-use-it-in-express",
      values: ["production", "development"]
    },
    {
      name: "OS",
      description: "The server operating system running OPEN-CUTS",
      values: ["Ubuntu 20.04 LTS Focal Fossa", "Ubuntu 18.04 LTS Bionic Beaver"]
    }
  ],
  logLink: "https://docs.open-cuts.org/logs",
  buildDiscovery: {
    engine: "gitlab",
    args: ["open-cuts%2Fopen-cuts"]
  }
};

const seedTests = systemId => [
  {
    name: "Install",
    description: "OPEN-CUTS can be installed using the Ansible Playbook",
    steps:
      "Follow the steps specified in the playbook's readme: https://gitlab.com/open-cuts/open-cuts-ansible",
    weight: "CRITICAL",
    system: systemId
  },
  {
    name: "Seed database",
    description: "A fresh OPEN-CUTS install will have its database seeded.",
    steps:
      "The database should contain an example system, with builds, tests, and runs.",
    system: systemId
  }
];

const seedBuilds = systemId => [
  {
    tag: "example-2",
    date: Date.now() - 1000,
    system: systemId
  },
  {
    tag: "example-1",
    date: Date.now() - 2000,
    system: systemId
  }
];

const seedUsers = [
  {
    name: "Alice",
    email: "alice@somewhere.nope",
    passwordHash: "fakehash",
    joined: Date.now() - 10000
  },
  {
    name: "Bob",
    email: "bob@somewhere.nope",
    passwordHash: "fakehash",
    joined: Date.now() - 10000
  },
  {
    name: "Mallory",
    email: "mallory@iamevil.nope",
    passwordHash: "fakehash",
    joined: Date.now() - 10000
  }
];

const seedLogs = [
  {
    name: "terminal output",
    content: "This is an example log. You can delte it."
  },
  {
    name: "~/.cache/open-cuts/open-cuts.log",
    content: "This is an example log. You can delte it."
  }
];

const seedRuns = ({
  test_install,
  test_seed,
  build_2,
  build_1,
  alice,
  bob,
  mallory
}) => [
  {
    comment: "Took approx. 20 minutes.",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "production"
      },
      {
        variable: "OS",
        value: "Ubuntu 20.04 LTS Focal Fossa"
      }
    ],
    test: test_install,
    build: build_2,
    user: alice
  },
  {
    comment: "Worked!",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "production"
      },
      {
        variable: "OS",
        value: "Ubuntu 18.04 LTS Bionic Beaver"
      }
    ],
    test: test_install,
    build: build_2,
    user: alice
  },
  {
    comment: "Took approx. 20 minutes.",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "development"
      },
      {
        variable: "OS",
        value: "Ubuntu 20.04 LTS Focal Fossa"
      }
    ],
    test: test_install,
    build: build_2,
    user: alice
  },
  {
    comment: "Worked!",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "development"
      },
      {
        variable: "OS",
        value: "Ubuntu 18.04 LTS Bionic Beaver"
      }
    ],
    test: test_install,
    build: build_2,
    user: alice
  },
  {
    comment: "Took approx. 20 minutes.",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "production"
      },
      {
        variable: "OS",
        value: "Ubuntu 20.04 LTS Focal Fossa"
      }
    ],
    test: test_install,
    build: build_2,
    user: bob
  },
  {
    comment: "Worked!",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "production"
      },
      {
        variable: "OS",
        value: "Ubuntu 18.04 LTS Bionic Beaver"
      }
    ],
    test: test_install,
    build: build_2,
    user: bob
  },
  {
    comment: "Took approx. 20 minutes.",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "development"
      },
      {
        variable: "OS",
        value: "Ubuntu 20.04 LTS Focal Fossa"
      }
    ],
    test: test_install,
    build: build_2,
    user: bob
  },
  {
    comment: "Worked!",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "development"
      },
      {
        variable: "OS",
        value: "Ubuntu 18.04 LTS Bionic Beaver"
      }
    ],
    test: test_install,
    build: build_2,
    user: bob
  },
  {
    comment: "Everything there!",
    result: "PASS",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "development"
      },
      {
        variable: "OS",
        value: "Ubuntu 18.04 LTS Bionic Beaver"
      }
    ],
    test: test_seed,
    build: build_2,
    user: bob
  },
  {
    comment: "I had to tweak the playbook to get it to work...",
    result: "WONKY",
    logs: seedLogs,
    combination: [
      {
        variable: "NODE_ENV",
        value: "production"
      },
      {
        variable: "OS",
        value: "Ubuntu 18.04 LTS Bionic Beaver"
      }
    ],
    test: test_install,
    build: build_1,
    user: bob
  },
  {
    comment:
      "I AM MALLORY AND I AM MALICIOUS! I did not actually test; i'm just reporting bullsh*t! Mwahahahaha!",
    result: "FAIL",
    combination: [
      {
        variable: "NODE_ENV",
        value: "production"
      },
      {
        variable: "OS",
        value: "Ubuntu 20.04 LTS Bionic Beaver"
      }
    ],
    test: test_install,
    build: build_1,
    user: mallory
  }
];

export const seedDatabase = () =>
  process.env.SEED
    ? Promise.all([
        SystemModel.estimatedDocumentCount()
          .then(number => number === 0)
          .then(needsSeeding =>
            needsSeeding
              ? SystemModel.create(seedSystem).then(({ _id }) =>
                  Promise.all([
                    ...seedTests(_id).map(x => TestModel.create(x)),
                    ...seedBuilds(_id).map(x => BuildModel.create(x)),
                    ...seedUsers.map(x => UserModel.create(x))
                  ])
                    .then(r => ({
                      test_install: r[0]._id,
                      test_seed: r[1]._id,
                      build_2: r[2]._id,
                      build_1: r[3]._id,
                      alice: r[4]._id,
                      bob: r[5]._id,
                      mallory: r[6]._id
                    }))
                    .then(refs =>
                      Promise.all(seedRuns(refs).map(r => RunModel.create(r)))
                    )
                    .then(() =>
                      UserModel.updateMany(
                        { confirm: { $exists: true } },
                        { $unset: { confirm: "" } }
                      )
                    )
                )
              : Promise.resolve()
          ),
        UserModel.findOne({
          email: process.env.SEED_EMAIL
        }).then(user =>
          user
            ? Promise.resolve()
            : UserModel.create({
                name: process.env.SEED_USER || "Admin",
                email: process.env.SEED_EMAIL,
                passwordHash: "fakehash",
                groups: ["ADMIN", "MOD", "REPORTER"]
              }).then(sendWelcomeAdminMail)
        )
      ])
    : Promise.resolve();
