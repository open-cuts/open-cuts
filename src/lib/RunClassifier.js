/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { RunModel } from "../models/run.js";
import NaiveBayesClassifier from "bayes-classifier";

/**
 * Naive Bayes classifier to assess the quality of reported runs and detect spam
 * TODO: This concept could be extended later to also learn from user ratings and grant xp based on that
 */
export default class RunClassifier {
  constructor() {
    this.classifier = new NaiveBayesClassifier();
    Promise.all([
      RunModel.find({ comment: { $ne: "" }, spam: true }).then(r =>
        this.classifier.addDocuments(
          [
            "Buy porn here: https://porn.xxx",
            "hot milfs in your area",
            "Join the bitcoin exchange",
            "fiat currency crypto",
            "elon musk",
            ...r.map(r => r.comment)
          ],
          "spam"
        )
      ),
      RunModel.find({ comment: { $ne: "" }, spam: false }).then(r =>
        this.classifier.addDocuments(
          ["Works", "No issues", "as expected", ...r.map(r => r.comment)],
          "ok"
        )
      )
    ]).then(() => this.classifier.train());
  }

  classify(run) {
    if (run.comment && this.classifier.classify(run.comment) === "spam") {
      return RunModel.findByIdAndUpdate(
        run.id,
        { flagged: true },
        { new: true }
      );
    } else {
      return run;
    }
  }

  /**
   * Mark a run as spam and re-train the ai if necessary
   *
   * @param {RunModel} run - run to flag
   * @returns {RunModel} run
   */
  spam(run) {
    // TODO punish spammer
    if (!run || run?.flagged || !run.comment) {
      return RunModel.findByIdAndDelete(run.id);
    } else {
      this.classifier.addDocument(run.comment, "spam");
      this.classifier.train();
      return RunModel.findByIdAndUpdate(
        run.id,
        { spam: true, $unset: { flagged: "" } },
        { new: true }
      );
    }
  }

  /**
   * Mark a run as NOT spam and re-train the ai if necessary
   *
   * @param {RunModel} run - run to flag
   * @returns {RunModel} run
   */
  ok(run) {
    // TODO make amends with wrongfully accused reporter
    if (!run || !run?.flagged) {
      return run;
    } else {
      if (run.comment) {
        this.classifier.addDocument(run.comment, "ok");
        this.classifier.train();
      }
      return RunModel.findByIdAndUpdate(
        run.id,
        { spam: false, $unset: { flagged: "" } },
        { new: true }
      );
    }
  }
}
