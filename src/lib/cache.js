/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { CacheModel } from "../models/cache.js";
import { BuildModel } from "../models/build.js";
import { dbEvent } from "./dbEvent.js";

const DAY = 86400000;

/**
 * @typedef args
 * @property {String} systemId
 * @property {String} buildId
 * @property {String} testId
 * @property {String} channel
 * @property {String} combination
 */

class Cache {
  /**
   * get a value from cache or, failing that, from the callback function
   * @param {String} fn function key
   * @param {args} args argument keys
   * @param {Function} callback function to retrieve data on cache miss
   * @returns {Promise<Object>}
   */
  get(fn, args, callback) {
    return CacheModel.findOne({ fn, args, ttl: { $gte: Date.now() } }).then(
      r =>
        r?.value ||
        callback().then(value =>
          value
            ? CacheModel.findOneAndReplace(
                { fn, args },
                { fn, args, ttl: Date.now() + DAY, value },
                { upsert: true, returnDocument: "after" }
              )
                .then(() => value)
                .catch(e => console.error(e) || value)
            : null
        )
    );
  }

  /**
   * Delete all matching cached documents. To be called if the underlying data changes.
   * @param {args} param0 select documents to delete
   * @returns {Promise}
   */
  delete({ systemId, buildId, testId, channel, combination }) {
    return CacheModel.deleteMany({
      $and: [
        systemId && { "args.systemId": { $eq: systemId } },
        buildId && { "args.buildId": { $eq: buildId } },
        channel && { "args.channel": { $eq: channel } },

        // delete anything test-agnostic or referencing a specified test, but nothing referencing other tests
        testId && { "args.testId": { $in: [testId, null] } },

        // delete anything combination-agnostic or referencing the specified combination, but nothing referencing other combinations
        combination && { "args.combination": { $in: [combination, null] } }
      ].filter(a => a)
    });
  }

  onRunChanged({ build, test, combination }) {
    return BuildModel.findById(build).then(({ channel, system }) =>
      this.delete({
        systemId: system.toString(),
        buildId: build.toString(),
        channel,
        testId: test.toString(),
        combination
      })
    );
  }

  onBuildChanged({ _id, channel, system }) {
    return Promise.all([
      this.delete({ systemId: system.toString(), channel }),
      this.delete({ buildId: _id.toString() })
    ]);
  }

  onTestChanged({ _id, system }) {
    return this.delete({ systemId: system.toString(), testId: _id.toString() });
  }
}

export const cache = new Cache();

dbEvent.on("onBuildChanged", cache.onBuildChanged.bind(cache));
dbEvent.on("onRunChanged", cache.onRunChanged.bind(cache));
dbEvent.on("onTestChanged", cache.onTestChanged.bind(cache));
