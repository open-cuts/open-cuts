/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Axios from "axios";
import { BuildModel } from "../models/build.js";
import { SystemModel } from "../models/system.js";
import { RunModel } from "../models/run.js";
import mongoose from "mongoose";
import { CacheModel } from "../models/cache.js";

// TODO allow per-system build retention strategies
const BUILDS_PER_CHANNEL = 20;
const MAX_BUILD_AGE = 365 * 2;

export async function housekeeping() {
  // delete outdated cached documents
  await CacheModel.deleteMany({ ttl: { $lte: Date.now() } }).then(
    ({ deletedCount }) =>
      console.log(`deleted ${deletedCount} outdated cached documents`)
  );
  await delay();

  // discover builds
  await SystemModel.find({
    "buildDiscovery.engine": { $exists: true }
  }).then(systems =>
    systems
      .reduce(async (prev, system) => {
        await prev;
        const builds = await buildDiscovery[system.buildDiscovery.engine](
          ...system.buildDiscovery.args
        );
        await builds.reduce(
          (prev, build) =>
            prev
              .then(() => createBuildIfNonexistent(build, system.id))
              .then(() => delay(5)),
          delay()
        ),
          delay();
      })
      .catch(() => {})
  );
  console.log(`build discovery complete`);
  await delay();

  // Delete outdated builds
  await BuildModel.deleteMany({ date: { $lt: calculateCutoffDate() } }).then(
    ({ deletedCount }) => console.log(`deleted ${deletedCount} outdated builds`)
  );
  await delay();

  // Cut channels off after fifth build
  await SystemModel.find().then(systems =>
    Promise.all(
      systems.map(({ id }) =>
        BuildModel.distinct("channel", { system: id }).then(channels =>
          Promise.all(
            channels.map(channel =>
              BuildModel.countDocuments({ channel, system: id }).then(count => {
                if (count > BUILDS_PER_CHANNEL) {
                  return BuildModel.findOneAndDelete({
                    channel,
                    system: id
                  }).sort({
                    tag: 1,
                    date: 1
                  });
                }
              })
            )
          )
        )
      )
    )
  );
  console.log(`channel cutoff complete`);
  await delay();

  // Throw out orphaned runs
  await RunModel.aggregate([
    { $project: { _id: 1, build: 1 } },
    {
      $lookup: {
        from: "builds",
        localField: "build",
        foreignField: "_id",
        as: "build"
      }
    },
    { $match: { $expr: { $eq: ["$build", []] } } }
  ])
    .then(ids =>
      RunModel.deleteMany({ _id: { $in: ids.map(({ _id }) => _id) } })
    )
    .then(({ deletedCount }) =>
      console.log(`deleted ${deletedCount} orphaned runs`)
    );
}

export function calculateCutoffDate() {
  return new Date().setDate(new Date().getDate() - MAX_BUILD_AGE);
}

export function createBuildIfNonexistent(build, id) {
  return BuildModel.exists({
    tag: build.tag,
    system: mongoose.Types.ObjectId(id)
  })
    .then(exists =>
      exists
        ? Promise.resolve()
        : BuildModel.create([
            {
              ...build,
              system: mongoose.Types.ObjectId(id)
            }
          ])
    )
    .catch(e => console.log(e));
}

export const buildDiscovery = {
  systemimage: (
    server = "http://system-image.ubports.com",
    referenceDevice = "bacon"
  ) =>
    Axios.get(`${server}/channels.json`)
      .then(({ data }) => Object.entries(data))
      .then(channels =>
        channels.filter(
          channel =>
            !channel[1].hidden &&
            !channel[1].redirect &&
            channel[1].devices[referenceDevice] && // HACK show channels for all devices once the ui is more mature
            !channel[0].endsWith("edge") // TODO bring back edge once tags are unambiguous https://github.com/ubports/system-image-server/blob/master/bin/import-images#L181-L208
        )
      )
      .then(channels =>
        channels.map(channel => [
          channel[0].replace("ubports-touch/16.04/", ""),
          channel[1].devices[referenceDevice].index
        ])
      )
      .then(channels =>
        Promise.all(
          channels.map(channel =>
            Axios.get(`${server}${channel[1]}`)
              .then(({ data }) =>
                data.images
                  .filter(i => i.type === "full")
                  .slice(-BUILDS_PER_CHANNEL)
              )
              .then(images =>
                images.map(i => ({
                  tag: getSystemImageVersionDetail(i.description, "tag"),
                  // tag: i.description.split(",").find(p => p.startsWith("tag=")).replace("tag=", ""),
                  date: parseSystemImageDate(i.description),
                  channel: channel[0]
                  // date: i.description.split(",").find(p => p.startsWith("ubports=")).replace("ubports=", "")
                }))
              )
              .then(images =>
                images.filter(i => i.date > calculateCutoffDate())
              )
          )
        )
      )
      .then(r => r.flat()),
  openstore: id =>
    Axios.get(`https://open-store.io/api/v4/apps/${id}?channel=xenial`)
      .then(
        ({ data }) => data.data.revisions.map(({ version }) => version) // easier filtering if mapped to object later
      )
      .then(
        builds =>
          [
            ...new Set(builds) // remove duplicates
          ]
            .slice(-BUILDS_PER_CHANNEL) // limit number of builds
            .map(tag => ({ tag })) // FIXME openstore does not provide release dates
      ),
  github: (owner, repo) =>
    gitLabOrGitHub(`https://api.github.com/repos/${owner}/${repo}/releases`),
  gitlab: id =>
    gitLabOrGitHub(`https://gitlab.com/api/v4/projects/${id}/releases`)
};

export function gitLabOrGitHub(endpoint) {
  return Axios.get(endpoint).then(({ data }) =>
    data
      .slice(0, BUILDS_PER_CHANNEL)
      .map(({ tag_name, created_at }) => ({
        tag: tag_name,
        date: new Date(created_at)
      }))
      .filter(i => i.date > calculateCutoffDate())
  );
}

export function getSystemImageVersionDetail(string, detail) {
  return string
    .split(",")
    .find(p => p.startsWith(`${detail}=`))
    .replace(`${detail}=`, "");
}

export function parseSystemImageDate(string) {
  const [date, time] = getSystemImageVersionDetail(string, "ubports").split(
    "-"
  );
  return new Date(
    Date.parse(
      date.substr(0, 4) + "/" + date.substr(4, 2) + "/" + date.substr(6, 2)
    ) + parseInt(time)
  );
}

async function delay(ms = 1000) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
