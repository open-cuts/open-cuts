/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import rateLimiter from "rate-limiter-flexible";
const { RateLimiterMongo } = rateLimiter;
import mongoose from "mongoose";

const rateLimiterMongo = new RateLimiterMongo({
  storeClient: mongoose.connection,
  points: 15, // tokens
  duration: 2 // per seconds
});

export const rateLimiterMiddleware = (req, res, next) => {
  rateLimiterMongo
    .consume(req.ip)
    .then(() => next())
    .catch(e => {
      // HACK https://github.com/animir/node-rate-limiter-flexible/issues/161
      if (e?.message?.includes("Cannot read properties of null")) next();
      else res.status(429).send("Too Many Requests");
    });
};
