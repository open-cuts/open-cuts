import test from "ava";
import * as td from "testdouble";

test.beforeEach(async t =>
  import("./dbEvent.js").then(ex => (t.context.modules = ex))
);

test("should export {dbEvent}", async t => {
  t.truthy(t.context.modules.dbEvent);
  const listener = td.func("listener");
  t.context.modules.dbEvent.on("listener", listener);
  t.context.modules.dbEvent.emit("listener");
  td.verify(listener());
  t.pass();
});
