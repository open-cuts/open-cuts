/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { gql } from "apollo-server-express";
import { ensureGroup } from "../lib/utils.js";

export const typeDefs = gql`
  type Mutation {
    "Create and return a new system property"
    createProperty(systemId: ID!, property: PropertyInput!): System

    "Update system property and return system"
    updateProperty(id: ID!, property: PropertyInput!): System

    "Delete a system property and return system"
    deleteProperty(id: ID!): System
  }

  "Properties that can take a range of pre-defined values, such as a package type or host environment."
  type Property {
    id: ID

    "Property name"
    name: String

    "Property description"
    description: String

    "Help link"
    link: String

    "Acceptable values"
    values: [String]
  }

  input PropertyInput {
    name: String
    description: String
    link: String
    values: [String]
  }
`;

export const resolvers = {
  Mutation: {
    createProperty: (_, { systemId, property }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.systems.createProperty(systemId, property)
      ),
    updateProperty: (_, { id, property }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.systems.updateProperty(id, property)
      ),
    deleteProperty: (_, { id }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.systems.deleteProperty(id)
      )
  },
  Property: {
    id: object => object._id ?? object.id
  }
};
