/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { gql } from "apollo-server-express";
import { ensureGroup } from "../lib/utils.js";

export const typeDefs = gql`
  type Query {
    "Get all builds"
    builds: [Build]

    "Get a single build"
    build(id: ID!): Build

    tensorSlice(
      buildId: ID!
      testId: ID!
      combination: [CombinationInput]
    ): ChartData
  }

  type Mutation {
    "Create and return a new build"
    createBuild(systemId: ID!, build: BuildInput!): Build

    "Update build metadata"
    updateBuild(id: ID!, build: BuildInput!): Build

    "Delete and return a build by ID"
    deleteBuild(id: ID!): Build
  }

  type Status {
    pass: Float
    wonky: Float
    fail: Float
    inconclusive: Float
  }

  enum Summary {
    FIXED
    PASS
    INCONCLUSIVE
    STILL_FAILING
    REGRESSION
  }

  type TestStatus {
    "test should be highlighted if the user did not run it yet for this build"
    highlight: Boolean
    test: Test
    status: Status
  }

  type CombinationStatus {
    combination: [Combination]
    status: Status
  }

  "A build of a system available for testing"
  type Build {
    id: ID
    tag: String
    date: String
    system: System
    channel: String
    "build should be highlighted if the user did not run all tests for this build yet"
    highlight: Boolean
    status: Status
    summary: Summary
    tests: [TestStatus]
    combinations: [CombinationStatus]
    prev: Build
    next: Build
    runs: [Run]
    runFeed(cursor: String, limit: Int): RunFeed
    chart: ChartData
  }

  input BuildInput {
    tag: String
    date: String
    channel: String
  }
`;

export const resolvers = {
  Query: {
    builds: (_, {}, context) => context.dataSources.builds.explore(),
    build: (_, { id }, context) => context.dataSources.builds.read(id),
    tensorSlice: (_, { buildId, testId, combination }, context) =>
      context.dataSources.builds.chart(buildId, testId, combination)
  },
  Mutation: {
    createBuild: (_, { systemId, build }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.builds.create({ system: systemId, ...build })
      ),
    updateBuild: (_, { id, build }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.builds.update(id, build)
      ),
    deleteBuild: (_, { id }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.builds.delete(id)
      )
  },
  Build: {
    tag: ({ id }, {}, context) =>
      context.dataSources.builds.read(id).then(r => r.tag),
    date: ({ id }, {}, context) =>
      context.dataSources.builds.read(id).then(r => r.date),
    system: ({ id }, {}, context) =>
      context.dataSources.builds.read(id).then(r => ({ id: r.system })),
    channel: ({ id }, {}, context) =>
      context.dataSources.builds.read(id).then(r => r.channel),
    highlight: ({ id }, {}, context) =>
      context.dataSources.builds.highlight(id),
    status: ({ id }, {}, context) => context.dataSources.builds.status(id),
    summary: ({ id }, {}, context) => context.dataSources.builds.summary(id),
    tests: ({ id }, {}, context) => context.dataSources.builds.tests(id),
    combinations: ({ id }, {}, context) =>
      context.dataSources.builds.combinations(id),
    prev: ({ id }, {}, context) => context.dataSources.builds.prev(id),
    next: ({ id }, {}, context) => context.dataSources.builds.next(id),
    runs: ({ id }, {}, context) => context.dataSources.builds.runs(id),
    runFeed: ({ id }, { cursor, limit }, context) =>
      context.dataSources.builds.runFeed(id, cursor, limit),
    chart: ({ id }, {}, context) => context.dataSources.builds.chart(id)
  }
};
