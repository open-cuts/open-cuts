import test from "ava";

test.beforeEach(async t =>
  import("./chartdata.js").then(chartdata => (t.context = chartdata))
);

test("exports {typeDefs, resolvers}", async t => {
  t.truthy(t.context.typeDefs);
  t.truthy(t.context.resolvers);
});
