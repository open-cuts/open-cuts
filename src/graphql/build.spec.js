import test from "ava";
import * as td from "testdouble";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./build.js"))
    .then(build => (t.context = build))
);

test("exports {typeDefs, resolvers}", async t => {
  t.truthy(t.context.typeDefs);
  t.truthy(t.context.resolvers);
});

test("{query {builds}}", async t => {
  const explore = td.func("explore");
  t.context.resolvers.Query.builds(
    undefined,
    {},
    {
      dataSources: { builds: { explore } }
    }
  );
  td.verify(explore());
  t.pass();
});
test("{query {build(id)}}", async t => {
  const read = td.func("read");
  t.context.resolvers.Query.build(
    undefined,
    { id: "a" },
    {
      dataSources: { builds: { read } }
    }
  );
  td.verify(read("a"));
  t.pass();
});
test("{query {tensorSlice(buildId, testId, combination)}}", async t => {
  const chart = td.func("chart");
  t.context.resolvers.Query.tensorSlice(
    undefined,
    {
      buildId: "buildId",
      testId: "testId",
      combination: "combination"
    },
    {
      dataSources: { builds: { chart } }
    }
  );
  td.verify(chart("buildId", "testId", "combination"));
  t.pass();
});
