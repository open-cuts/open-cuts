/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as gqlToolsMerge from "@graphql-tools/merge";

/**
 * Provides schema stitching functionality without breaking everything, like Apollo
 * so very *very* VERY often does.
 *
 * @param  {...String} modules - node module ids, should export {typeDefs, resolvers}
 * @returns {Promise} - Promise for {resolvers, typeDefs} to be used in ApolloServer
 */
const awesomeStitcher = (...modules) =>
  Promise.all(modules.map(module => import(module))).then(resolvedModules => ({
    resolvers: gqlToolsMerge.mergeResolvers(
      resolvedModules.map(i => i.resolvers)
    ),
    typeDefs: gqlToolsMerge.mergeTypeDefs(resolvedModules.map(i => i.typeDefs))
  }));

/**
 * Resolves { resolvers, typeDefs }
 */
export default async () =>
  await awesomeStitcher(
    "./build.js",
    "./chartdata.js",
    "./property.js",
    "./run.js",
    "./stats.js",
    "./system.js",
    "./test.js",
    "./user.js"
  );
