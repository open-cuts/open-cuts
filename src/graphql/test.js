/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { gql } from "apollo-server-express";
import { ensureGroup } from "../lib/utils.js";

export const typeDefs = gql`
  type Query {
    "Get all tests"
    tests: [Test]

    "Get a single test"
    test(id: ID!): Test
  }

  type Mutation {
    "Create and return a new test"
    createTest(systemId: ID!, test: TestInput!): Test

    "Update test metadata"
    updateTest(id: ID!, test: TestInput!): Test

    "Delete and return a test by ID"
    deleteTest(id: ID!): Test
  }

  type TestGroup {
    group: String
    tests: [Test]
  }

  "Determines how the score impacts the parent score."
  enum Weight {
    "If this test fails, so does the parent"
    CRITICAL

    "High (5x) impact on the parent score"
    HIGH

    "Medium (1x) impact on the parent score"
    MEDIUM

    "Low (.5x) impact on the parent score"
    LOW
  }

  "A test defined on a system"
  type Test {
    id: ID

    "Test name"
    name: String

    "Test description"
    description: String

    "Test group"
    group: String

    "Test steps"
    steps: String

    "Test weight"
    weight: Weight

    "System under test"
    system: System

    runs: [Run]
  }

  input TestInput {
    name: String
    description: String
    group: String
    steps: String
    weight: Weight
  }
`;

export const resolvers = {
  Query: {
    tests: (_, {}, context) => context.dataSources.tests.explore(),
    test: (_, { id }, context) => context.dataSources.tests.read(id)
  },
  Mutation: {
    createTest: (_, { systemId, test }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.tests.create({ system: systemId, ...test })
      ),
    updateTest: (_, { id, test }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.tests.update(id, test)
      ),
    deleteTest: (_, { id }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.tests.delete(id)
      )
  },
  Test: {
    name: ({ id }, {}, context) =>
      context.dataSources.tests.read(id).then(r => r.name),
    description: ({ id }, {}, context) =>
      context.dataSources.tests.read(id).then(r => r.description),
    group: ({ id }, {}, context) =>
      context.dataSources.tests.read(id).then(r => r.group),
    steps: ({ id }, {}, context) =>
      context.dataSources.tests.read(id).then(r => r.steps),
    weight: ({ id }, {}, context) =>
      context.dataSources.tests.read(id).then(r => r.weight),
    system: ({ id }, {}, context) =>
      context.dataSources.tests.read(id).then(r => ({ id: r.system })),
    runs: ({ id }, {}, context) => context.dataSources.tests.runs(id)
  }
};
