import test from "ava";
import * as td from "testdouble";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./run.js"))
    .then(run => (t.context = run))
);

test("exports {typeDefs, resolvers}", async t => {
  t.truthy(t.context.typeDefs);
  t.truthy(t.context.resolvers);
});
