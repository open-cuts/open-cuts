/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { gql } from "apollo-server-express";
import {
  ensureGroup,
  ensureSelfOrAdmin,
  ensureGuest,
  sendConfirmationMail,
  hashPassword,
  sendTokenMail
} from "../lib/utils.js";
import * as uuid from "uuid";

export const typeDefs = gql`
  type Query {
    "Get all users"
    users: [User]

    "Get a single user"
    user(id: ID!): User

    "Get yourself, requires \`authentication\` header"
    me: User

    "Get token after logging in"
    login(email: String!, password: String!): String
  }

  type Mutation {
    "Create and return a new user"
    createUser(user: UserInput!): User

    "Update build metadata"
    updateUser(id: ID!, user: UserInput!): User

    "Delete and return a build by ID"
    deleteUser(id: ID!): User

    "Update own token, requires \`authentication\` header"
    regenerateToken: String

    "Set user groups"
    setUserGroups(id: ID!, groups: [Group!]!): User

    "Get token after logging in"
    register(email: String!, password: String!, name: String!): User

    "Confirm email"
    confirmEmail(email: String!, password: String!, code: String!): String

    "Update password"
    updatePassword(password: String!): User

    "Forgot password"
    forgotPassword(email: String!): User
  }

  "User group"
  enum Group {
    "allowed to create reports"
    REPORTER

    "allowed to moderate reports and view logs"
    MOD

    "full administrator rights"
    ADMIN
  }

  "A user"
  type User {
    id: ID
    name: String
    email: String
    joined: String
    token: String
    groups: [Group]
    runs: [Run]
    activity: Int
  }

  input UserInput {
    name: String
    email: String
  }
`;

export const resolvers = {
  Query: {
    users: (_, {}, context) =>
      context.dataSources.users.find({ confirm: { $exists: false } }),
    user: (_, { id }, context) => context.dataSources.users.read(id),
    me: (_, {}, context) =>
      ensureGuest(context)
        .then(() => null)
        .catch(() => context.dataSources.users.read(context.user.id)),
    login: (_, { email, password }, context) =>
      ensureGuest(context).then(() =>
        context.dataSources.users
          .findOne({
            email,
            passwordHash: hashPassword(password),
            confirm: { $exists: false }
          })
          .then(user => user.token)
          .catch(() =>
            Promise.reject(
              new Error(
                "Wrong email/password combination or unverified account!"
              )
            )
          )
      )
  },
  Mutation: {
    createUser: (_, { user }, context) =>
      ensureGroup(context, "ADMIN").then(() =>
        context.dataSources.users.create(user)
      ),
    updateUser: (_, { id, user }, context) =>
      ensureSelfOrAdmin(context, id).then(() =>
        context.dataSources.users.update(id, user)
      ),
    deleteUser: (_, { id }, context) =>
      ensureSelfOrAdmin(context, id).then(() =>
        context.dataSources.users.delete(id)
      ),
    regenerateToken: (_, {}, context) =>
      context.dataSources.users
        .update(context.user.id, { token: uuid.v4() })
        .then(user => user.token),
    setUserGroups: (_, { id, groups }, context) =>
      ensureGroup(context, "ADMIN").then(() =>
        context.dataSources.users.update(id, { groups })
      ),
    register: (_, { email, password, name }, context) =>
      ensureGuest(context).then(() =>
        password.length <= 10
          ? Promise.reject(new Error("Your password is too weak!"))
          : context.dataSources.users
              .create({
                name,
                email,
                passwordHash: hashPassword(password)
              })
              .then(user => {
                sendConfirmationMail(user);
                return user;
              })
      ),
    confirmEmail: (_, { email, password, code }, context) =>
      ensureGuest(context).then(() =>
        context.dataSources.users
          .findOneAndUpdate(
            {
              email,
              passwordHash: hashPassword(password),
              confirm: code
            },
            { $unset: { confirm: "" } },
            { new: true }
          )
          .then(user => user.token)
          .catch(() =>
            Promise.reject(
              new Error("Wrong email, password or confirmation code!")
            )
          )
      ),
    updatePassword: (_, { password }, context) =>
      password.length <= 10
        ? Promise.reject(new Error("Your password is too weak!"))
        : context.dataSources.users.update(context.user.id, {
            passwordHash: hashPassword(password)
          }),
    forgotPassword: (_, { email }, context) =>
      context.dataSources.users.findOne({ email }).then(user => {
        sendTokenMail(user);
        return user;
      })
  },
  User: {
    name: ({ id }, {}, context) =>
      context.dataSources.users.read(id).then(r => r.name),
    email: ({ id }, {}, context) =>
      context.dataSources.users.read(id).then(r =>
        ensureSelfOrAdmin(context, r.id)
          .then(() => r.email)
          .catch(() => null)
      ),
    joined: ({ id }, {}, context) =>
      context.dataSources.users.read(id).then(r => r.joined),
    token: ({ id }, {}, context) =>
      context.dataSources.users.read(id).then(r =>
        ensureSelfOrAdmin(context, r.id)
          .then(() => r.token)
          .catch(() => null)
      ),
    groups: ({ id }, {}, context) =>
      context.dataSources.users.read(id).then(r => r.groups),
    runs: ({ id }, {}, context) => context.dataSources.runs.find({ user: id }),
    activity: ({ id }, {}, context) =>
      context.dataSources.runs.countDocuments({ user: id })
  }
};
