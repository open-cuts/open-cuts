import test from "ava";
import * as td from "testdouble";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./user.js"))
    .then(user => (t.context = user))
);

test("exports {typeDefs, resolvers}", async t => {
  t.truthy(t.context.typeDefs);
  t.truthy(t.context.resolvers);
});
