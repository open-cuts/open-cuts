/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { gql } from "apollo-server-express";
import { ensureGroup, ensureSelfOrMod } from "../lib/utils.js";

export const typeDefs = gql`
  type Query {
    "Return all runs"
    runs: [Run]!

    "Find Run by id"
    run(id: ID!): Run

    "Return flagged runs"
    spamReviewQueue: [Run]!
  }

  type Mutation {
    "Create and return a new run"
    createRun(testId: ID!, buildId: ID!, run: RunInput!): Run

    "Update run metadata"
    updateRun(id: ID!, run: RunInput!): System

    "Delete and return a run by ID"
    deleteRun(id: ID!): Run

    "Create and return a new run and be smart about it :)"
    smartRun(testId: ID!, systemId: ID!, tag: String!, run: RunInput!): Run

    "Mark spam"
    spam(id: ID!): Run

    "Mark not spam"
    notSpam(id: ID!): Run
  }

  "A test run"
  type Run {
    id: ID
    date: String
    result: Result
    comment: String
    logs: [Log]
    combination: [Combination]
    test: Test
    build: Build
    system: System
    user: User
  }

  input RunInput {
    date: String
    result: Result
    comment: String
    logs: [LogInput]
    combination: [CombinationInput]
  }

  type RunFeed {
    cursor: String
    count: Int
    runs: [Run]
  }

  type Log {
    id: ID
    name: String
    content: String
  }

  input LogInput {
    name: String
    content: String
  }

  type Combination {
    variable: String
    value: String
  }

  input CombinationInput {
    variable: String
    value: String
  }

  "Result of a test run"
  enum Result {
    "Everything works as expected"
    PASS

    "Test partly successful, minor inconveniences"
    WONKY

    "Test unsuccessful, major flaws"
    FAIL

    "Not applicable"
    NA
  }
`;

export const resolvers = {
  Query: {
    runs: (_, {}, context) => context.dataSources.runs.explore(),
    run: (_, { id }, context) => context.dataSources.runs.read(id),
    spamReviewQueue: (_, {}, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.runs.find({ flagged: true })
      )
  },
  Mutation: {
    createRun: (_, { testId, buildId, run }, context) =>
      context.dataSources.runs
        .create({
          test: testId,
          build: buildId,
          user: context?.user?.id,
          ...run
        })
        .then(run => global.runClassifier.classify(run)),
    updateRun: (_, { id, run }, context) =>
      ensureGroup(context, "MOD")
        .then(() => context.dataSources.runs.update(id, run))
        .then(run => global.runClassifier.classify(run)),
    deleteRun: (_, { id }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.runs.delete(id)
      ),
    smartRun: (_, { testId, systemId, tag, run }, context) =>
      context.dataSources.builds
        .findOne({ tag, system: systemId })
        .then(build =>
          build
            ? context.dataSources.runs
                .create({
                  test: testId,
                  build: build.id,
                  user: context?.user?.id,
                  ...run
                })
                .then(run => global.runClassifier.classify(run))
            : new Error("No such build")
        )
        .catch(error => new Error(`Failed to create run: ${error}`)),
    spam: (_, { id }, context) =>
      ensureGroup(context, "MOD")
        .then(() => context.dataSources.runs.read(id))
        .then(run => global.runClassifier.spam(run)),
    notSpam: (_, { id }, context) =>
      ensureGroup(context, "MOD")
        .then(() => context.dataSources.runs.read(id))
        .then(run => global.runClassifier.ok(run))
  },
  Run: {
    date: ({ id }, {}, context) =>
      context.dataSources.runs.read(id).then(r => Date.parse(r.date)), // TODO create a datatype or directive for this
    result: ({ id }, {}, context) =>
      context.dataSources.runs.read(id).then(r => r.result),
    comment: ({ id }, {}, context) =>
      context.dataSources.runs.read(id).then(r =>
        !(r?.spam || r?.flagged)
          ? r?.comment
          : ensureSelfOrMod(context, r.user?.id)
              .then(() => r?.comment)
              .catch(() => "*** held for moderation ***")
      ),
    logs: ({ id }, {}, context) =>
      context.dataSources.runs.read(id).then(r =>
        ensureSelfOrMod(context, r.user?.id)
          .then(() => r.logs)
          .catch(() =>
            r.logs.map(l => ({
              id: l._id,
              name: l.name,
              content: "*** only visible to moderators ***"
            }))
          )
      ),
    combination: ({ id }, {}, context) =>
      context.dataSources.runs.read(id).then(r => r.combination),
    test: ({ id }, {}, context) =>
      context.dataSources.runs.read(id).then(r => ({ id: r.test })),
    build: ({ id }, {}, context) =>
      context.dataSources.runs.read(id).then(r => ({ id: r.build })),
    system: ({ id }, {}, context) =>
      context.dataSources.runs
        .read(id)
        .then(r =>
          context.dataSources.builds.read(r.build).then(r => ({ id: r.system }))
        ),
    user: ({ id }, {}, context) =>
      context.dataSources.runs
        .read(id)
        .then(r => (r.user ? { id: r.user } : null))
  }
};
