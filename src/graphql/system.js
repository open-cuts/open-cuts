/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { gql } from "apollo-server-express";
import { ensureGroup } from "../lib/utils.js";

export const typeDefs = gql`
  type Query {
    "Return all Systems"
    systems: [System]!

    "Find system by id"
    system(id: ID!): System
  }

  type Mutation {
    "Create and return a new system"
    createSystem(system: SystemInput!): System!

    "Update system metadata"
    updateSystem(id: ID!, system: SystemInput!): System!

    "Delete and return a system by ID"
    deleteSystem(id: ID!): System!

    setCombinationFilter(id: ID!, combinationFilter: String!): System
  }

  type Channel {
    channel: String
    builds: [Build]
  }

  type BuildDiscovery {
    engine: String
    args: [String]
  }

  input BuildDiscoveryInput {
    engine: String
    args: [String]
  }

  "A system under test"
  type System {
    "Unique system id"
    id: ID

    "Name of the system"
    name: String

    "Short string describing the system"
    description: String

    "A preview image or logo to illustrate the system."
    image: String

    buildDiscovery: BuildDiscovery

    "Builds available to test"
    builds: [Build]

    "Latest build"
    latestBuild: Build

    "Most recent builds grouped by channel"
    channels: [Channel]

    "Tests defined on this system."
    tests: [Test]

    "Tests defined on this system grouped by... well, grouped by group."
    groups: [TestGroup]

    "System properties"
    properties: [Property]

    "Link to information about logs"
    logLink: String

    "OCLF expression"
    combinationFilter: String
  }

  input SystemInput {
    "Name of the system"
    name: String

    "System description"
    description: String

    "System preview image"
    image: String

    "System properties"
    properties: [PropertyInput]

    "Link to information about logs"
    logLink: String

    buildDiscovery: BuildDiscoveryInput
  }
`;

export const resolvers = {
  Query: {
    systems: (_, {}, context) => context.dataSources.systems.explore(),
    system: (_, { id }, context) => context.dataSources.systems.read(id)
  },
  Mutation: {
    createSystem: (_, { system }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.systems.create(system)
      ),
    updateSystem: (_, { id, system }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.systems.update(id, system)
      ),
    deleteSystem: (_, { id }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.systems.delete(id)
      ),
    setCombinationFilter: (_, { id, combinationFilter }, context) =>
      ensureGroup(context, "MOD").then(() =>
        context.dataSources.systems.setCombinationFilter(id, combinationFilter)
      )
  },
  System: {
    name: ({ id }, {}, context) =>
      context.dataSources.systems.read(id).then(r => r.name),
    description: ({ id }, {}, context) =>
      context.dataSources.systems.read(id).then(r => r.description),
    image: ({ id }, {}, context) =>
      context.dataSources.systems.read(id).then(r => r.image),
    properties: ({ id }, {}, context) =>
      context.dataSources.systems.read(id).then(r => r.properties),
    logLink: ({ id }, {}, context) =>
      context.dataSources.systems.read(id).then(r => r.logLink),
    builds: ({ id }, {}, context) => context.dataSources.systems.builds(id),
    latestBuild: ({ id }, {}, context) =>
      context.dataSources.systems.latestBuild(id),
    channels: ({ id }, {}, context) => context.dataSources.systems.channels(id),
    tests: ({ id }, {}, context) => context.dataSources.systems.tests(id),
    groups: ({ id }, {}, context) => context.dataSources.systems.groups(id),
    combinationFilter: ({ id }, {}, context) =>
      context.dataSources.systems
        .read(id)
        .then(r => r.combinationFilter || "{}")
        .catch(() => "{}")
  }
};
