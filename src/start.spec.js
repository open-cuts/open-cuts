import test from "ava";
import * as td from "testdouble";

import dotenv from "dotenv";

test("should start server", async t => {
  td.replace(dotenv, "config", td.func());
  const exit = td.func("exit");
  td.replace(process, "exit", exit);
  await t.notThrowsAsync(async () => (await import("./start.js")).default);
  td.verify(exit(666));
});
