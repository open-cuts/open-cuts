/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import mongoose from "mongoose";
import {
  defaultSchemaOptions,
  generateConfirmationCode
} from "../lib/utils.js";
import * as uuid from "uuid";

const userSchema = mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    passwordHash: { type: String, required: true },
    confirm: {
      type: String,
      default: generateConfirmationCode
    },
    joined: {
      type: Date,
      default: Date.now
    },
    token: {
      type: String,
      default: uuid.v4,
      required: true,
      unique: true
    },
    groups: { type: Array, default: ["REPORTER"] }
  },
  defaultSchemaOptions
);
export const UserModel = mongoose.model("User", userSchema);
