import test from "ava";
import * as td from "testdouble";

const fakeId = "000000000000000000000000";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./test.js"))
    .then(({ TestModel }) => (t.context = { TestModel }))
);

test("exports {TestModel}", async t => {
  t.truthy(t.context.TestModel);
});
test("should accept valid test", async t => {
  const test = new t.context.TestModel({
    system: fakeId,
    name: "Test all the things"
  });
  await t.notThrowsAsync(test.validate());
  t.truthy(test.system);
  t.truthy(test.name);
});
test("should reject invalid test", async t => {
  const test = new t.context.TestModel();
  const error = await t.throwsAsync(test.validate());
  t.is(
    error.message,
    "Test validation failed: system: Path `system` is required., name: Path `name` is required."
  );
});
