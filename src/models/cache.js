/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import mongoose from "mongoose";

const DAY = 86400000;

const argsSchema = mongoose.Schema(
  {
    systemId: { type: String },
    buildId: { type: String },
    testId: { type: String },
    channel: { type: String },
    combination: { type: Object }
  },
  { _id: false }
);

const cacheSchema = mongoose.Schema({
  fn: { type: String, required: true },
  args: { type: argsSchema },
  value: { type: Object, required: true },
  ttl: { type: Number, default: () => Date.now() + DAY }
});
export const CacheModel = mongoose.model("Cache", cacheSchema, "cache");
