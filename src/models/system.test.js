import test from "ava";
import * as td from "testdouble";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./system.js"))
    .then(({ SystemModel }) => (t.context = { SystemModel }))
);

test("exports {SystemModel}", async t => {
  t.truthy(t.context.SystemModel);
});
test("should accept valid system", async t => {
  const system1 = new t.context.SystemModel({
    name: "My boring system",
    description: "Description"
  });
  const system2 = new t.context.SystemModel({
    name: "My awesome system",
    description: "Description",
    properties: [
      {
        name: "My input",
        description: "My input description"
      },
      {
        name: "My true checkbox",
        description: "My true checkbox description",
        values: ["true", "false"]
      },
      {
        name: "My false checkbox",
        description: "My false checkbox description",
        values: ["false", "true"]
      },
      {
        name: "My select",
        description: "My select description",
        values: ["one", "two", "three"]
      }
    ]
  });
  await t.notThrowsAsync(system1.validate());
  await t.notThrowsAsync(system2.validate());
});
test("should reject invalid system", async t => {
  const system = new t.context.SystemModel();
  const error = await t.throwsAsync(system.validate());
  t.is(
    error.message,
    "System validation failed: description: Path `description` is required., name: Path `name` is required."
  );
});
