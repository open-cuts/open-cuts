import test from "ava";
import * as td from "testdouble";

const fakeId = "000000000000000000000000";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./cache.js"))
    .then(({ CacheModel }) => (t.context = { CacheModel }))
);

test("exports {CacheModel}", async t => {
  t.truthy(t.context.CacheModel);
});
test("should accept valid cache", async t => {
  const cache = new t.context.CacheModel({ fn: "test", value: fakeId });
  await t.notThrowsAsync(cache.validate());
  t.is(cache.fn, "test");
});
