/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import mongoose from "mongoose";
import { defaultSchemaOptions } from "../lib/utils.js";
import { dbEvent } from "../lib/dbEvent.js";

const runSchema = mongoose.Schema(
  {
    date: { type: Date, default: Date.now },
    comment: String,
    result: {
      type: String,
      default: "PASS",
      enum: ["PASS", "WONKY", "FAIL"]
    },
    spam: { type: Boolean },
    flagged: { type: Boolean },
    logs: [{ name: String, content: String }],
    combination: [{ variable: String, value: String }],
    test: { type: "ObjectId", ref: "Test", required: true },
    build: {
      type: "ObjectId",
      ref: "Build",
      required: true
    },
    user: { type: "ObjectId", ref: "User" }
  },
  defaultSchemaOptions
);
runSchema.post("save", run => dbEvent.emit("onRunChanged", run));
runSchema.post("remove", run => dbEvent.emit("onRunChanged", run));

export const RunModel = mongoose.model("Run", runSchema);
