import test from "ava";
import * as td from "testdouble";

const fakeId = "000000000000000000000000";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./build.js"))
    .then(({ BuildModel }) => (t.context = { BuildModel }))
);

test("exports {BuildModel}", async t => {
  t.truthy(t.context.BuildModel);
});
test("should accept valid build", async t => {
  const build = new t.context.BuildModel({ tag: "1.3.37", system: fakeId });
  await t.notThrowsAsync(build.validate());
  t.is(build.channel, "default");
  t.is(build.tag, "1.3.37");
  t.truthy(build.system);
  t.truthy(build.date);
});
test("should reject invalid build", async t => {
  const build = new t.context.BuildModel();
  const error = await t.throwsAsync(build.validate());
  t.is(
    error.message,
    "Build validation failed: system: Path `system` is required., tag: Path `tag` is required."
  );
});
