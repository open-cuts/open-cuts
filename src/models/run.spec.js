import test from "ava";
import * as td from "testdouble";

const fakeId = "000000000000000000000000";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./run.js"))
    .then(({ RunModel }) => (t.context = { RunModel }))
);

test("exports {RunModel}", async t => {
  t.truthy(t.context.RunModel);
});
test("should accept valid run", async t => {
  const run = new t.context.RunModel({
    user: fakeId,
    build: fakeId,
    test: fakeId
  });
  await t.notThrowsAsync(run.validate());
  t.is(run.result, "PASS");
  t.truthy(run.user);
  t.truthy(run.build);
  t.truthy(run.test);
});
test("should reject invalid run", async t => {
  const run = new t.context.RunModel();
  const error = await t.throwsAsync(run.validate());
  t.is(
    error.message,
    "Run validation failed: build: Path `build` is required., test: Path `test` is required."
  );
});
