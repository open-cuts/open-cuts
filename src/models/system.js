/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import mongoose from "mongoose";
import { defaultSchemaOptions } from "../lib/utils.js";

const systemSchema = mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    description: { type: String, required: true },
    image: String,
    properties: [
      {
        name: { type: String, required: true },
        description: { type: String, required: true },
        link: String,
        values: [String] // select, checkbox if [true, false], input if empty
      }
    ],
    logLink: String,
    combinationFilter: String,
    buildDiscovery: { engine: String, args: [String] }
  },
  defaultSchemaOptions
);
export const SystemModel = mongoose.model("System", systemSchema);
