import test from "ava";
import * as td from "testdouble";

const fakeId = "000000000000000000000000";

test.beforeEach(async t =>
  td
    .replaceEsm("../lib/utils.js")
    .then(() => import("./user.js"))
    .then(({ UserModel }) => (t.context = { UserModel }))
);

test("exports {UserModel}", async t => {
  t.truthy(t.context.UserModel);
});
test("should accept valid user", async t => {
  const user = new t.context.UserModel({
    name: "Alice",
    email: "alice@bob.org",
    passwordHash: 1337
  });
  await t.notThrowsAsync(user.validate());
  t.is(user.name, "Alice");
  t.is(user.email, "alice@bob.org");
});
test("should reject invalid user", async t => {
  const user = new t.context.UserModel();
  const error = await t.throwsAsync(user.validate());
  t.is(
    error.message,
    "User validation failed: passwordHash: Path `passwordHash` is required., email: Path `email` is required., name: Path `name` is required."
  );
});
