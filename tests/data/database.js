/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2022 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import mongodb from "mongodb";
const { ObjectId } = mongodb;

export const data = {
  builds: [
    {
      _id: ObjectId("000000000000000000000000"),
      channel: "stable",
      system: ObjectId("200000000000000000000000"),
      tag: "1.3.37",
      date: "1587907707212"
    },
    {
      _id: ObjectId("000000000000000000000001"),
      channel: "stable",
      system: ObjectId("200000000000000000000000"),
      tag: "1.3.42",
      date: "1587907707213"
    },
    {
      _id: ObjectId("000000000000000000000002"),
      channel: "dev",
      system: ObjectId("200000000000000000000000"),
      tag: "1.3.37-dev",
      date: "1587907707214"
    },
    {
      _id: ObjectId("000000000000000000000003"),
      channel: "stable",
      system: ObjectId("200000000000000000000000"),
      tag: "1.3.69",
      date: "1587907707215"
    },
    {
      _id: ObjectId("000000000000000000000004"),
      channel: "stable",
      system: ObjectId("200000000000000000000001"),
      tag: "42",
      date: "1587907707216"
    },
    {
      _id: ObjectId("000000000000000000000005"),
      channel: "default",
      system: ObjectId("200000000000000000000002"),
      tag: "0.1",
      date: "1587907707216"
    }
  ],
  runs: [
    {
      _id: ObjectId("100000000000000000000000"),
      result: "PASS",
      test: ObjectId("300000000000000000000000"),
      build: ObjectId("000000000000000000000001"),
      user: ObjectId("400000000000000000000000"),
      date: "1587907707212",
      comment: "works",
      logs: [],
      combination: [
        { variable: "Boolean", value: "false" },
        { variable: "List", value: "c" }
      ]
    },
    {
      _id: ObjectId("100000000000000000000001"),
      result: "PASS",
      test: ObjectId("300000000000000000000002"),
      build: ObjectId("000000000000000000000005"),
      user: ObjectId("400000000000000000000000"),
      date: "1587907707212",
      comment: "works",
      logs: [],
      combination: [{ variable: "List", value: "a" }]
    }
  ],
  systems: [
    {
      _id: ObjectId("200000000000000000000000"),
      name: "System 0",
      description: "A great system",
      image: "https://spri.nz/system.png",
      properties: [
        {
          _id: ObjectId("500000000000000000000000"),
          description: "",
          name: "Boolean",
          values: ["true", "false"]
        },
        {
          _id: ObjectId("500000000000000000000001"),
          description: "",
          name: "List",
          values: ["a", "b", "c"]
        }
      ],
      combinationFilter: JSON.stringify({
        IMPLIES: [
          { CHECK: ["Boolean", ["true"]] },
          { CHECK: ["List", ["a", "b"]] }
        ]
      })
    },
    {
      _id: ObjectId("200000000000000000000001"),
      name: "System 1"
    },
    {
      _id: ObjectId("200000000000000000000002"),
      name: "System 2",
      properties: [
        {
          _id: ObjectId("520000000000000000000000"),
          description: "",
          name: "List",
          values: ["a", "b"]
        }
      ]
    }
  ],
  tests: [
    {
      _id: ObjectId("300000000000000000000000"),
      weight: "MEDIUM",
      system: ObjectId("200000000000000000000000"),
      name: "Some test",
      description: "Do something with the system",
      __v: 0
    },
    {
      _id: ObjectId("300000000000000000000001"),
      weight: "HIGH",
      system: ObjectId("200000000000000000000000"),
      name: "Some other test",
      description: "Do something else",
      __v: 0
    },
    {
      _id: ObjectId("300000000000000000000002"),
      weight: "HIGH",
      system: ObjectId("200000000000000000000002"),
      name: "A great test",
      description: "Do the thing",
      __v: 0
    }
  ],
  users: [
    {
      _id: ObjectId("400000000000000000000000"),
      groups: ["ADMIN", "MOD", "REPORTER"],
      joined: "1594642769179",
      name: "Johannah Sprinz",
      email: "noreply@open-cuts.org",
      passwordHash:
        "cca2272df09bddadb1aa34645c9345077aed6d41e88dbbca5551f3d94338887c",
      token: "b4321625-411f-4014-be70-ed8122fd12fa",
      __v: 0
    }
  ]
};
