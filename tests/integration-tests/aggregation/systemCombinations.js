/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sinon from "sinon";
import chai from "chai";
const expect = chai.expect;

import { SystemModel } from "../../../src/models/system.js";
import { systemCombinations } from "../../../src/aggregation/systems.js";

export default () =>
  describe("systemCombinations", function () {
    it("should resolve system combinations for system with two axes", function (done) {
      SystemModel.aggregate(systemCombinations("200000000000000000000000"))
        .then(r => {
          expect(r[0].combinations).to.have.lengthOf(6);
          done();
        })
        .catch(done);
    });
    it("should resolve system combinations for system with one axis", function (done) {
      SystemModel.aggregate(systemCombinations("200000000000000000000002"))
        .then(r => {
          expect(r[0].combinations).to.have.deep.members([
            [{ value: "a", variable: "List" }],
            [{ value: "b", variable: "List" }]
          ]);
          done();
        })
        .catch(done);
    });
    it("should resolve system combinations for system with no axes", function (done) {
      SystemModel.aggregate(systemCombinations("200000000000000000000001"))
        .then(r => {
          done(r[0].combinations);
        })
        .catch(done);
    });
    it("should apply simple combination filter", function (done) {
      SystemModel.aggregate(
        systemCombinations("200000000000000000000000", {
          CHECK: ["List", ["a", "b"]]
        })
      )
        .then(r => {
          expect(r[0].combinations).to.have.lengthOf(4);
          done();
        })
        .catch(done);
    });
    it("should apply complex combination filter", function (done) {
      SystemModel.aggregate(
        systemCombinations("200000000000000000000000", {
          AND: [
            {
              IMPLIES: [
                { CHECK: ["Boolean", ["true"]] },
                { CHECK: ["List", ["a", "b"]] }
              ]
            },
            {
              OR: [
                { CHECK: ["List", ["a"]] },
                { CHECK: ["Boolean", ["false"]] }
              ]
            }
          ]
        })
      )
        .then(r => {
          expect(r[0].combinations).to.have.lengthOf(4);
          done();
        })
        .catch(done);
    });
  });
