/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sinon from "sinon";
import chai from "chai";
const expect = chai.expect;
import mongoUnit from "mongo-unit";

import mongoose from "mongoose";
import { data } from "../../data/database.js";

import systemCombinations from "./systemCombinations.js";
import fullMatrix from "./fullMatrix.js";
import testCombinationMatrixForBuild from "./testCombinationMatrixForBuild.js";
import testMatrixForBuild from "./testMatrixForBuild.js";
import combinationMatrixForBuild from "./combinationMatrixForBuild.js";
import buildStatus from "./buildStatus.js";

export default () =>
  describe("MongoDB Aggregation pipelines", function () {
    after(function () {
      return mongoose.disconnect().then(() => mongoose.disconnect());
    });

    before(function () {
      this.timeout(false);
      return mongoose.connect(process.env.MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
      });
    });

    beforeEach(function () {
      return mongoUnit.load(data);
    });

    afterEach(function () {
      return mongoUnit.drop();
    });

    systemCombinations();
    fullMatrix();
    testCombinationMatrixForBuild();
    testMatrixForBuild();
    combinationMatrixForBuild();
    buildStatus();
  });
