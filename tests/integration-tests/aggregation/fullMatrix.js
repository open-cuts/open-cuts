/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sinon from "sinon";
import chai from "chai";
const expect = chai.expect;

import { SystemModel } from "../../../src/models/system.js";
import { fullMatrix } from "../../../src/aggregation/systems.js";

const responseCleaner = r => JSON.parse(JSON.stringify(r));

export default () =>
  describe("fullMatrix", function () {
    it("should resolve build-test-matrix for system with one axis", function (done) {
      SystemModel.aggregate(fullMatrix("200000000000000000000002"))
        .then(r => {
          expect(responseCleaner(r)).to.have.deep.members([
            {
              test: { id: "300000000000000000000002" },
              build: { id: "000000000000000000000005" },
              status: { pass: 0.5, wonky: 0, fail: 0, inconclusive: 0.5 },
              combination: [{ variable: "List", value: "a" }]
            },
            {
              test: { id: "300000000000000000000002" },
              build: { id: "000000000000000000000005" },
              status: { pass: 0, wonky: 0, fail: 0, inconclusive: 1 },
              combination: [{ variable: "List", value: "b" }]
            }
          ]);
          done();
        })
        .catch(done);
    });
    it("should resolve build-test-matrix for all builds by default", function (done) {
      SystemModel.aggregate(fullMatrix("200000000000000000000000"))
        .then(r => {
          expect(r).to.have.lengthOf(48);
          expect(
            r.filter(
              f =>
                f.status.pass == 0 &&
                f.status.wonky == 0 &&
                f.status.fail == 0 &&
                f.status.inconclusive == 1
            )
          ).to.have.lengthOf(47);
          expect(
            r.filter(
              f =>
                f.status.pass == 0.5 &&
                f.status.wonky == 0 &&
                f.status.fail == 0 &&
                f.status.inconclusive == 0.5
            )
          ).to.have.lengthOf(1);
          done();
        })
        .catch(done);
    });
    it("should resolve build-test-matrix for a single build if specified", function (done) {
      SystemModel.aggregate(
        fullMatrix("200000000000000000000000", "000000000000000000000001")
      )
        .then(r => {
          expect(r).to.have.lengthOf(12);
          expect(
            r.filter(
              f =>
                f.status.pass == 0 &&
                f.status.wonky == 0 &&
                f.status.fail == 0 &&
                f.status.inconclusive == 1
            )
          ).to.have.lengthOf(11);
          expect(
            r.filter(
              f =>
                f.status.pass == 0.5 &&
                f.status.wonky == 0 &&
                f.status.fail == 0 &&
                f.status.inconclusive == 0.5
            )
          ).to.have.lengthOf(1);
          done();
        })
        .catch(done);
    });
    it("should resolve build-test-matrix for a single test if specified", function (done) {
      SystemModel.aggregate(
        fullMatrix("200000000000000000000000", null, "300000000000000000000000")
      )
        .then(r => {
          expect(r).to.have.lengthOf(24);
          expect(
            r.filter(
              f =>
                f.status.pass == 0 &&
                f.status.wonky == 0 &&
                f.status.fail == 0 &&
                f.status.inconclusive == 1
            )
          ).to.have.lengthOf(23);
          expect(
            r.filter(
              f =>
                f.status.pass == 0.5 &&
                f.status.wonky == 0 &&
                f.status.fail == 0 &&
                f.status.inconclusive == 0.5
            )
          ).to.have.lengthOf(1);
          done();
        })
        .catch(done);
    });
  });
