/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sinon from "sinon";
import chai from "chai";
const expect = chai.expect;

import { SystemModel } from "../../../src/models/system.js";
import { testMatrixForBuild } from "../../../src/aggregation/systems.js";

const fakeId = "000000000000000000000000";
const responseCleaner = r => JSON.parse(JSON.stringify(r));

export default () =>
  describe("testMatrixForBuild", function () {
    it("should resolve test-matrix for a build", function (done) {
      SystemModel.aggregate(
        testMatrixForBuild(
          "200000000000000000000000",
          "000000000000000000000001"
        )
      )
        .then(r => {
          expect(responseCleaner(r)).to.have.deep.members([
            {
              test: { id: "300000000000000000000000" },
              status: {
                pass: 0.08333333333333333,
                wonky: 0,
                fail: 0,
                inconclusive: 0.9166666666666666
              }
            },
            {
              test: { id: "300000000000000000000001" },
              status: { pass: 0, wonky: 0, fail: 0, inconclusive: 1 }
            }
          ]);
          done();
        })
        .catch(done);
    });
  });
