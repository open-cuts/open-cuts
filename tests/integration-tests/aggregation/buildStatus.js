/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sinon from "sinon";
import chai from "chai";
const expect = chai.expect;

import { SystemModel } from "../../../src/models/system.js";
import { buildStatus } from "../../../src/aggregation/systems.js";

const fakeId = "000000000000000000000000";

export default () =>
  describe("buildStatus", function () {
    it("should resolve build status for system with two axes", function (done) {
      SystemModel.aggregate(
        buildStatus("200000000000000000000000", "000000000000000000000001")
      )
        .then(r => {
          expect(r).to.deep.eql([
            {
              pass: 0.041666666666666664,
              wonky: 0,
              fail: 0,
              inconclusive: 0.9583333333333334
            }
          ]);
          done();
        })
        .catch(done);
    });
    it("should resolve build status for system with one axis", function (done) {
      SystemModel.aggregate(
        buildStatus("200000000000000000000002", "000000000000000000000005")
      )
        .then(r => {
          expect(r).to.deep.eql([
            {
              pass: 0.25,
              wonky: 0,
              fail: 0,
              inconclusive: 0.75
            }
          ]);
          done();
        })
        .catch(done);
    });
  });
