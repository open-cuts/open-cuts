/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sinon from "sinon";
import chai from "chai";
const expect = chai.expect;

import { SystemModel } from "../../../src/models/system.js";
import { combinationMatrixForBuild } from "../../../src/aggregation/systems.js";

const fakeId = "000000000000000000000000";
const responseCleaner = r => JSON.parse(JSON.stringify(r));

export default () =>
  describe("combinationMatrixForBuild", function () {
    it("should resolve combination-matrix for a build", function (done) {
      SystemModel.aggregate(
        combinationMatrixForBuild(
          "200000000000000000000000",
          "000000000000000000000001"
        )
      )
        .then(r => {
          expect(r).to.have.lengthOf(6);
          expect(
            r.filter(
              f =>
                f.status.pass == 0 &&
                f.status.wonky == 0 &&
                f.status.fail == 0 &&
                f.status.inconclusive == 1
            )
          ).to.have.lengthOf(5);
          expect(
            r.filter(
              f =>
                f.status.pass == 0.25 &&
                f.status.wonky == 0 &&
                f.status.fail == 0 &&
                f.status.inconclusive == 0.75
            )
          ).to.have.lengthOf(1);
          done();
        })
        .catch(done);
    });
  });
