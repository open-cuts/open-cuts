/**
 * This file is part of OPEN-CUTS, the open crowdsourced user-testing suite.
 * Copyright (C) 2020-2021 Johannah Sprinz <neo@neothethird.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sinon from "sinon";
import chai from "chai";
import sinonChai from "sinon-chai";
chai.use(sinonChai);
const expect = chai.expect;

process.env.PORT = 4002;

import { server } from "../../src/server.js";
import mongoUnit from "mongo-unit";
import axios from "axios";
import { data } from "../data/database.js";
import { seedDatabase } from "../../src/lib/seeder.js";
import { SystemModel } from "../../src/models/system.js";
import { UserModel } from "../../src/models/user.js";

const defaultQuery = {
  url: `http://localhost:${process.env.PORT}/graphql`,
  method: "post"
};

const anonymousRequest = query =>
  axios({
    ...defaultQuery,
    data: { query }
  });

const adminRequest = query =>
  axios({
    ...defaultQuery,
    headers: {
      authorization: "b4321625-411f-4014-be70-ed8122fd12fa"
    },
    data: { query }
  });

after(function () {
  return mongoUnit.stop();
});

before(function () {
  this.timeout(false);
  return mongoUnit
    .start({ version: "latest", platform: "linux" })
    .then(() => {
      process.env.SECRET = "supersecure";
      process.env.MONGODB_URL = mongoUnit.getUrl();
    })
    .then(() => server());
});

// FIXME: Some of these could and should be unit tests
describe("GraphQL", function () {
  after(function () {
    global.killServer();
  });

  beforeEach(function () {
    return mongoUnit.load(data);
  });

  afterEach(function () {
    return mongoUnit.drop();
  });

  describe("Seeder", function () {
    it("should not seed if database already initialized", function () {
      const oldEnv = process.env;
      process.env.SEED = true;
      process.env.SEED_EMAIL = "info@open-cuts.org";
      process.env.SEED_USER = "Amelia Earhart";
      global.smtp = {
        sendMail: sinon.fake.resolves()
      };
      return seedDatabase()
        .then(() => anonymousRequest(`{ builds { id tag } }`))
        .then(result => {
          expect(result.data.data.builds).to.have.lengthOf(6);
          expect(global.smtp.sendMail).to.have.been.called;
          delete global.smtp;
          process.env = oldEnv;
        });
    });
    it("should not create seed user if they exist", function () {
      const oldEnv = process.env;
      process.env.SEED = true;
      process.env.SEED_EMAIL = "info@open-cuts.org";
      global.smtp = {
        sendMail: sinon.fake.resolves()
      };
      return UserModel.create({
        email: process.env.SEED_EMAIL,
        name: "Alexandria Ocasio-Cortez",
        passwordHash: "fakehash"
      })
        .then(() => seedDatabase())
        .then(() => anonymousRequest(`{ builds { id tag } }`))
        .then(result => {
          expect(result.data.data.builds).to.have.lengthOf(6);
          expect(global.smtp.sendMail).to.not.have.been.called;
          delete global.smtp;
          process.env = oldEnv;
        });
    });
    it("should seed if no systems exist", function () {
      const oldEnv = process.env;
      process.env.SEED = true;
      process.env.SEED_EMAIL = "info@open-cuts.org";
      process.env.SEED_USER = "Noam Chomsky";
      global.smtp = {
        sendMail: sinon.fake.resolves()
      };
      return SystemModel.deleteMany({ name: { $exists: true } })
        .then(() => seedDatabase())
        .then(() => anonymousRequest(`{ builds { id tag } }`))
        .then(result => {
          expect(result.data.data.builds).to.have.lengthOf(8);
          expect(global.smtp.sendMail).to.have.been.called;
          delete global.smtp;
          process.env = oldEnv;
        });
    });
  });

  describe("Queries", function () {
    describe("builds", function () {
      it("should resolve builds", function (done) {
        anonymousRequest(`{ builds { id tag } }`)
          .then(result => {
            expect(result.data.data.builds).to.eql([
              { id: "000000000000000000000000", tag: "1.3.37" },
              { id: "000000000000000000000001", tag: "1.3.42" },
              { id: "000000000000000000000002", tag: "1.3.37-dev" },
              { id: "000000000000000000000003", tag: "1.3.69" },
              { id: "000000000000000000000004", tag: "42" },
              { id: "000000000000000000000005", tag: "0.1" }
            ]);
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("build", function () {
      it("should resolve build", function (done) {
        adminRequest(`{
            build(id: "000000000000000000000001") {
              id
              tag
              system {name}
              status {pass wonky fail inconclusive}
              summary
              date
              highlight
              channel
              runs {result}
              tests {
                test { name }
                highlight
                status { pass wonky fail inconclusive }
              }
              combinations {
                combination { variable value }
                status { pass wonky fail inconclusive }
              }
              prev { id }
              next { id }
              chart {
                labels
                datasets {
                  label
                  backgroundColor
                  data
                }
              }
            }
          }`)
          .then(result => {
            expect(result.data.data.build).to.deep.include({
              id: "000000000000000000000001",
              tag: "1.3.42",
              system: { name: "System 0" },
              prev: { id: "000000000000000000000000" },
              next: { id: "000000000000000000000003" },
              status: {
                pass: 0.05,
                wonky: 0,
                fail: 0,
                inconclusive: 0.95
              },
              highlight: true,
              summary: "INCONCLUSIVE",
              runs: [{ result: "PASS" }]
            });
            expect(result.data.data.build.chart).to.deep.eql({
              labels: ["1.3.37", "1.3.42", "1.3.69"],
              datasets: [
                {
                  label: "pass",
                  backgroundColor: "green",
                  data: [0, 0.05, 0]
                },
                { label: "wonky", backgroundColor: "yellow", data: [0, 0, 0] },
                { label: "fail", backgroundColor: "red", data: [0, 0, 0] },
                {
                  label: "inconclusive",
                  backgroundColor: "grey",
                  data: [1, 0.95, 1]
                }
              ]
            });
            expect(result.data.data.build.tests).to.have.deep.members([
              {
                test: { name: "Some test" },
                highlight: false,
                status: {
                  pass: 0.1,
                  wonky: 0,
                  fail: 0,
                  inconclusive: 0.9
                }
              },
              {
                test: { name: "Some other test" },
                highlight: true,
                status: {
                  pass: 0,
                  wonky: 0,
                  fail: 0,
                  inconclusive: 1
                }
              }
            ]);
            expect(result.data.data.build.combinations).to.have.deep.members([
              {
                combination: [
                  { variable: "Boolean", value: "true" },
                  { variable: "List", value: "a" }
                ],
                status: { pass: 0, wonky: 0, fail: 0, inconclusive: 1 }
              },
              {
                combination: [
                  { variable: "Boolean", value: "false" },
                  { variable: "List", value: "b" }
                ],
                status: { pass: 0, wonky: 0, fail: 0, inconclusive: 1 }
              },
              {
                combination: [
                  { variable: "Boolean", value: "false" },
                  { variable: "List", value: "c" }
                ],
                status: { pass: 0.25, wonky: 0, fail: 0, inconclusive: 0.75 }
              },
              {
                combination: [
                  { variable: "Boolean", value: "false" },
                  { variable: "List", value: "a" }
                ],
                status: { pass: 0, wonky: 0, fail: 0, inconclusive: 1 }
              },
              {
                combination: [
                  { variable: "Boolean", value: "true" },
                  { variable: "List", value: "b" }
                ],
                status: { pass: 0, wonky: 0, fail: 0, inconclusive: 1 }
              }
            ]);
            done(result.data.errors);
          })
          .catch(done);
      });

      it("should resolve null for prev if none exists", function (done) {
        anonymousRequest(`{
            build(id: "000000000000000000000000") {
              id
              prev { id }
              next { id }
              highlight
              chart {
                labels
                datasets {
                  label
                  backgroundColor
                  data
                }
              }
            }
          }`)
          .then(result => {
            expect(result.data.data.build).to.eql({
              id: "000000000000000000000000",
              prev: null,
              next: { id: "000000000000000000000001" },
              highlight: false,
              chart: {
                labels: ["1.3.37", "1.3.42"],
                datasets: [
                  {
                    label: "pass",
                    backgroundColor: "green",
                    data: [0, 0.05]
                  },
                  { label: "wonky", backgroundColor: "yellow", data: [0, 0] },
                  { label: "fail", backgroundColor: "red", data: [0, 0] },
                  {
                    label: "inconclusive",
                    backgroundColor: "grey",
                    data: [1, 0.95]
                  }
                ]
              }
            });
            done(result.data.errors);
          })
          .catch(done);
      });

      it("should resolve null for prev and next if none exist", function (done) {
        anonymousRequest(`{
            build(id: "000000000000000000000002") {
              id
              prev { id }
              next { id }
            }
          }`)
          .then(result => {
            expect(result.data.data.build).to.eql({
              id: "000000000000000000000002",
              prev: null,
              next: null
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });

    describe("tensorSlice", function () {
      it("should resolve tensor slice", function (done) {
        anonymousRequest(`{
            tensorSlice(
              buildId: "000000000000000000000000",
              testId: "300000000000000000000000",
              combination: [
                { variable: "Boolean", value: "false" },
                { variable: "List", value: "c" }
              ]
            ) {
              labels
              datasets {
                label
                backgroundColor
                data
              }
            }
          }`)
          .then(result => {
            expect(result.data.data.tensorSlice).to.deep.eql({
              labels: ["1.3.37", "1.3.42"],
              datasets: [
                {
                  backgroundColor: "green",
                  data: [0, 0.5],
                  label: "pass"
                },
                {
                  backgroundColor: "yellow",
                  data: [0, 0],
                  label: "wonky"
                },
                {
                  backgroundColor: "red",
                  data: [0, 0],
                  label: "fail"
                },
                {
                  backgroundColor: "grey",
                  data: [1, 0.5],
                  label: "inconclusive"
                }
              ]
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });

    describe("runs", function () {
      it("should resolve runs", function (done) {
        anonymousRequest(`{ runs { id date } }`)
          .then(result => {
            expect(result.data.data.runs).to.eql([
              { id: "100000000000000000000000", date: "1587907707000" },
              { id: "100000000000000000000001", date: "1587907707000" }
            ]);
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("run", function () {
      it("should resolve run", function (done) {
        anonymousRequest(`{
            run(id: "100000000000000000000000") {
              id
              date
              user { name }
            }
          }`)
          .then(result => {
            expect(result.data.data.run).to.eql({
              date: "1587907707000",
              id: "100000000000000000000000",
              user: { name: "Johannah Sprinz" }
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("stats", function () {
      it("should resolve stats", function (done) {
        anonymousRequest(`{
            stats {
              builds { total }
              users { total }
            }
          }`)
          .then(result => {
            expect(result.data.data.stats).to.eql({
              builds: {
                total: 6
              },
              users: {
                total: 1
              }
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("spamReviewQueue", function () {
      it("should reject unauthenticated request", function (done) {
        anonymousRequest(`{ spamReviewQueue { id } }`)
          .then(result => {
            expect(result.data.errors[0].message).to.include(
              "Insufficient permissions"
            );
            done();
          })
          .catch(done);
      });
      it("should resolve empty spam review queue", function (done) {
        adminRequest(`{ spamReviewQueue { id } }`)
          .then(result => {
            expect(result.data.data.spamReviewQueue).to.eql([]);
            done(result.data.errors);
          })
          .catch(done);
      });
      it("should resolve spam review queue");
    });

    describe("systems", function () {
      it("should resolve systems", function (done) {
        anonymousRequest(`{ systems { id name } }`)
          .then(result => {
            expect(result.data.data.systems).to.deep.eql([
              { id: "200000000000000000000000", name: "System 0" },
              { id: "200000000000000000000001", name: "System 1" },
              { id: "200000000000000000000002", name: "System 2" }
            ]);
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("system", function () {
      it("should resolve system", function (done) {
        anonymousRequest(`{
          system(id: "200000000000000000000000") {
            name
            description
            image
            builds {
              tag
            }
            properties {id name description values }
            channels { channel builds { tag } }
            tests {
              name
              runs {
                date
                user {
                  name
                }
              }
            }
          }
        }`)
          .then(result => {
            expect(result.data.data.system).to.deep.eql({
              builds: [
                { tag: "1.3.37" },
                { tag: "1.3.42" },
                { tag: "1.3.37-dev" },
                { tag: "1.3.69" }
              ],
              channels: [
                {
                  builds: [
                    {
                      tag: "1.3.37-dev"
                    }
                  ],
                  channel: "dev"
                },
                {
                  builds: [
                    {
                      tag: "1.3.69"
                    },
                    {
                      tag: "1.3.42"
                    },
                    {
                      tag: "1.3.37"
                    }
                  ],
                  channel: "stable"
                }
              ],
              description: "A great system",
              image: "https://spri.nz/system.png",
              name: "System 0",
              properties: [
                {
                  description: "",
                  id: "500000000000000000000000",
                  name: "Boolean",
                  values: ["true", "false"]
                },
                {
                  description: "",
                  id: "500000000000000000000001",
                  name: "List",
                  values: ["a", "b", "c"]
                }
              ],
              tests: [
                {
                  name: "Some test",
                  runs: [
                    {
                      date: "1587907707000",
                      user: { name: "Johannah Sprinz" }
                    }
                  ]
                },
                {
                  name: "Some other test",
                  runs: []
                }
              ]
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });

    describe("tests", function () {
      it("should resolve tests", function (done) {
        anonymousRequest(`{ tests { id name } }`)
          .then(result => {
            expect(result.data.data.tests).to.eql([
              { id: "300000000000000000000000", name: "Some test" },
              { id: "300000000000000000000001", name: "Some other test" },
              { id: "300000000000000000000002", name: "A great test" }
            ]);
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("test", function () {
      it("should resolve test", function (done) {
        anonymousRequest(`{
            test(id: "300000000000000000000000") {
              id name description weight system { name }
            }
          }`)
          .then(result => {
            expect(result.data.data.test).to.eql({
              id: "300000000000000000000000",
              name: "Some test",
              description: "Do something with the system",
              system: { name: "System 0" },
              weight: "MEDIUM"
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });

    describe("users", function () {
      it("should resolve users", function (done) {
        anonymousRequest(`{ users { id name } }`)
          .then(result => {
            expect(result.data.data.users).to.eql([
              {
                id: "400000000000000000000000",
                name: "Johannah Sprinz"
              }
            ]);
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("user", function () {
      it("should mask private info", function (done) {
        anonymousRequest(`{
            user(id: "400000000000000000000000") {
              id name token email groups joined activity runs {id}
            }
          }`)
          .then(result => {
            expect(result.data.data.user).to.eql({
              id: "400000000000000000000000",
              name: "Johannah Sprinz",
              token: null,
              email: null,
              groups: ["ADMIN", "MOD", "REPORTER"],
              joined: "1594642769179",
              activity: 2,
              runs: [
                { id: "100000000000000000000000" },
                { id: "100000000000000000000001" }
              ]
            });
            done(result.data.errors);
          })
          .catch(done);
      });
      it("should display private info to admins", function (done) {
        adminRequest(`{
            user(id: "400000000000000000000000") {
              token email
            }
          }`)
          .then(result => {
            expect(result.data.data.user).to.eql({
              token: "b4321625-411f-4014-be70-ed8122fd12fa",
              email: "noreply@open-cuts.org"
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });

    describe("me", function () {
      it("should resolve null without token", function (done) {
        anonymousRequest(`{ me { name } }`)
          .then(result => {
            expect(result.data.data.me).to.eql(null);
            done(result.data.errors);
          })
          .catch(done);
      });
      it("should resolve self with valid token", function (done) {
        adminRequest(`{ me { name } }`)
          .then(result => {
            expect(result.data.data.me).to.eql({
              name: "Johannah Sprinz"
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });

    describe("login", function () {
      it("should withstand NoSQL injections", function (done) {
        anonymousRequest(`{ login(email: "{$ne: ""}", password: "{$ne: ""}") }`)
          .then(result => done(`should have rejected but got ${result}`))
          .catch(() => done());
      });
      it("should reject invalid credentials", function (done) {
        anonymousRequest(`{ login(
              email: "a@b.com",
              password: "wasd1337bombombom"
            )
          }`)
          .then(result => {
            expect(result.data.errors[0].message).includes(
              "Wrong email/password combination"
            );
            done();
          })
          .catch(done);
      });
      it("should resolve token for valid credentials", function (done) {
        anonymousRequest(`{ login(
              email: "noreply@open-cuts.org",
              password: "secure"
            )
          }`)
          .then(result => {
            expect(result.data.data.login).to.eql(
              "b4321625-411f-4014-be70-ed8122fd12fa"
            );
            done(result.data.errors);
          })
          .catch(done);
      });
    });
  });

  describe("Mutations", function () {
    describe("createBuild", function () {
      it("should reject anonymous request", function (done) {
        anonymousRequest(`mutation {
          createBuild(
            systemId: "200000000000000000000000"
            build: {
              tag: "42",
              date: "1234",
              channel: "stable"
            }
          ) { tag system { name }}
        }`)
          .then(result => {
            expect(result.data.errors[0].message).to.include(
              "Insufficient permissions"
            );
            done(result.data.errors.length !== 1);
          })
          .catch(done);
      });
      it("should create build", function (done) {
        adminRequest(`mutation {
          createBuild(
            systemId: "200000000000000000000000"
            build: {
              tag: "42",
              date: "1234",
              channel: "stable"
            }
          ) { tag system { name }}
        }`)
          .then(result => {
            expect(result.data.data.createBuild).to.eql({
              tag: "42",
              system: {
                name: "System 0"
              }
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("createRun", function () {
      it("should create anonymous run", function (done) {
        anonymousRequest(`mutation {
          createRun(
            testId: "300000000000000000000000"
            buildId: "000000000000000000000000"
            run: {
              result: PASS,
              comment: "fine",
              logs: [{name: "main", content: "error 42: everything exploded"}]
            }
          ) { comment result user { name } test { name } build { tag } logs {name content}}
        }`)
          .then(result => {
            expect(result.data.data.createRun).to.eql({
              comment: "fine",
              result: "PASS",
              user: null,
              test: { name: "Some test" },
              build: { tag: "1.3.37" },
              logs: [
                {
                  name: "main",
                  content: "*** only visible to moderators ***"
                }
              ]
            });
            done(result.data.errors);
          })
          .catch(done);
      });
      it("should create user run", function (done) {
        adminRequest(`mutation {
          createRun(
            testId: "300000000000000000000000"
            buildId: "000000000000000000000000"
            run: {
              result: WONKY,
              comment: "not great",
              logs: [{name: "main", content: "bonk! something funky happened"}]
            }
          ) { comment result user { name } logs {content}}
        }`)
          .then(result => {
            expect(result.data.data.createRun).to.eql({
              comment: "not great",
              result: "WONKY",
              logs: [
                {
                  content: "bonk! something funky happened"
                }
              ],
              user: { name: "Johannah Sprinz" }
            });
            done(result.data.errors);
          })
          .catch(done);
      });
    });
    describe("smartRun", function () {
      it("should create anonymous run", function (done) {
        anonymousRequest(`mutation {
          smartRun(
            testId: "300000000000000000000000"
            systemId: "200000000000000000000000"
            tag: "1.3.37"
            run: {
              result: PASS,
              comment: "fine",
              logs: [{name: "main", content: "error 42: everything exploded"}]
            }
          ) { comment result user { name } test { name } build { tag } logs {name content}}
        }`)
          .then(result => {
            expect(result.data.data.smartRun).to.eql({
              comment: "fine",
              result: "PASS",
              user: null,
              test: { name: "Some test" },
              build: { tag: "1.3.37" },
              logs: [
                {
                  name: "main",
                  content: "*** only visible to moderators ***"
                }
              ]
            });
            done(result.data.errors);
          })
          .catch(done);
      });
      it("should reject invalid tag", function (done) {
        adminRequest(`mutation {
          smartRun(
            testId: "300000000000000000000000"
            systemId: "200000000000000000000000"
            tag: "not a real tag"
            run: {
              result: PASS,
              comment: "fine",
              logs: [{name: "main", content: "error 42: everything exploded"}]
            }
          ) { comment result user { name } test { name } build { tag } logs {name content}}
        }`)
          .then(result => {
            expect(result.data.errors).to.have.lengthOf(1);
            expect(result.data.errors[0].message).to.eql("No such build");
            done(!result.data.errors);
          })
          .catch(done);
      });
    });
  });
});
