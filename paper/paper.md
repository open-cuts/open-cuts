---
title: "OPEN-CUTS: open crowdsourced user-testing suite"
tags:
  - Human Computation
  - Crowdsourcing
  - Software Quality Assurance
  - Open-Source Software Development
authors:
  - name: Johannah Sprinz
    orcid: 0000-0002-6425-7054
    affiliation: "1, 2"
affiliations:
  - name: LMU Munich
    index: 1
  - name: UBports Foundation
    index: 2
date: 01 March 2022
bibliography: paper.bib
---

<!--
docker run --rm --volume $PWD/paper:/data --user $(id -u):$(id -g) --env JOURNAL=joss openjournals/paperdraft paper.md
-->

**WIP! Will be submitted to [JOSS](https://joss.theoj.org/) when the time comes. Check out [@sprinz_leveraging_2022] for intermediate results.**

# Summary

<!-- TODO -->

# Statement of need

OPEN-CUTS is a human computation platform for crowdsourcing user-tests in open-source communities.

![screenshot of the landing page](https://spri.nz/images/open-cuts.png)

OPEN-CUTS was designed so it would suite the needs of most open-source communities. It has already been evaluated in the UBports Community, which develops the Ubuntu Touch mobile operating system [@sprinz_leveraging_2022].

<!-- TODO describe use in the ubports installer -->

# Acknowledgements

Special acknowledgment belongs to François Bry, Sebastian Mader, and Nils Heller from the Teaching and Research Unit Programming and Modelling Languages at LMU Munich and Florian Leeber, Marius Gripsgård, and Dalton Durst from the UBports Foundation for their conceptual contributions during the genesis of this project.

# References
