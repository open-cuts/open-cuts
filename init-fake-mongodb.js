#!/bin/env -S node --experimental-modules
import mongoUnit from "mongo-unit";

const run = async () => {
  await mongoUnit.start({ version: "4.2.6", platform: "linux", port: 1337 });
  await mongoUnit.stop();
};

run()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
