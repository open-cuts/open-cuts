# OPEN-CUTS

**THIS PROJECT IS STILL IN DEVELOPMENT! DO NOT USE FOR PRODUCTION!**

The api server for [OPEN-CUTS, the open crowdsourced user-testing suite](https://www.open-cuts.org). Served by [Express](http://expressjs.com/) to provide a [GraphQL API](https://graphql.org/) by [Apollo-Server](https://www.apollographql.com/docs/apollo-server/v1/servers/express/). The underlying [MongoDB](https://www.mongodb.com/) is queried by [Mongoose](https://mongoosejs.com/).

## Setup

Run `npm install` to install the dependencies and create an `.env` file:

```
OPENCUTS_FRONTEND_URL=https://whereever.you.are.hosting.it/
MONGODB_URL=mongodb://localhost/my_database
SMTP_CONFIG=<nodemailer config in json format, see https://nodemailer.com/smtp/>
SECRET=<random secret for hashing>
ADMIN_EMAIL=<a user registering with this email will be made an admin>
```

The development server can be started by running `npm start`. A graphql playground will be available at [http://localhost:4001/graphql](http://localhost:4001/graphql).

## License

Original development by [Johannah Sprinz](https://spri.nz).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
